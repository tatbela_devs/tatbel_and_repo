package com.tatbela.Presenters

import android.content.Context
import com.tatbela.interfaces.BaseListener
import com.tatbela.interfaces.Function
import com.tatbela.view_interfaces.BaseViewInterface

/**
 * Created by 3bdoelnaggar on 7/5/18.
 */
open class BasePresenter(viewInterface: BaseViewInterface, context: Context) : BaseListener {

    protected var viewInterface: BaseViewInterface? = viewInterface
    protected var context: Context?= context

    override fun onError(throwable: Throwable){
        if (viewInterface is BaseViewInterface){
            viewInterface!!.onError(throwable)
        }
    }

}
