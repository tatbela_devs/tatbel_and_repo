package com.tatbela.Presenters

import android.content.Context
import com.tatbela.api.Model
import com.tatbela.api.UserInfoManager

/**
 * Created by 3bdoelnaggar on 7/5/18.
 */
class ComplainPresenter(context: Context) {


    private val userInfoManager = UserInfoManager(context)


    public fun getComplains(onSuccess: (ArrayList<Model.Complain>) -> Unit, onError: (Throwable) -> Unit) {
        userInfoManager.getComplains(onSuccess, onError)
    }

    public fun getComplainById(id: String, onSuccess: (Model.ComplainWithComments) -> Unit, onError: (Throwable) -> Unit) {
        userInfoManager.getComplainById(id, onSuccess, onError)
    }

    public fun submitComplain(onSuccess: (Model.ResponseSwagger) -> Unit, onError: (Throwable) -> Unit) {
        userInfoManager.submitComplain(onSuccess, onError)

    }
}