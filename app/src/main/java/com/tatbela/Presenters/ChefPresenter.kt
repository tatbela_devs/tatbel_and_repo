package com.tatbela.Presenters

import android.content.Context
import com.tatbela.api.ChefManager
import com.tatbela.api.Model

/**
 * Created by 3bdoelnaggar on 7/5/18.
 */
class ChefPresenter(context: Context) {


    private val chefManager = ChefManager(context)

    public fun getChefsNearBy(lat: Double, lng: Double, onSuccess: (ArrayList<Model.Chef>) -> Unit, onError: (Throwable) -> Unit) {
        chefManager.getChefsNearBy(lat, lng, onSuccess, onError)
    }

    public fun getChefById(id: Int, onSuccess:(Model.ChefWithRate)-> Unit, onError: (Throwable) -> Unit) {
        chefManager.getChefById(id, onSuccess, onError)
    }

    public fun getOurChefs(countryId: Int, cityId: Int, areaId: Int, onSuccess: (ArrayList<Model.ChefWithRate>) -> Unit, onError: (Throwable) -> Unit) {
        chefManager.getOurChefs(countryId, cityId, areaId, onSuccess, onError)
    }

}