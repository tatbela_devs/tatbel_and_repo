package com.tatbela.Presenters

import android.content.Context
import com.tatbela.api.Model
import com.tatbela.api.UserInfoManager

/**
 * Created by 3bdoelnaggar on 7/5/18.
 */
class AddressesPresenter(context: Context, private val onSuccess: (result: ArrayList<Model.AddressSwaggerWithId>) -> Unit, val onError: (Throwable) -> Unit) {


    private val userInfoManager = UserInfoManager(context)
    public fun getAddresses() {
        userInfoManager.getAddresses(onSuccess, onError)
    }
}