package com.tatbela.Presenters

import android.content.Context
import com.tatbela.api.GeneralInfoManager
import com.tatbela.api.Model

/**
 * Created by 3bdoelnaggar on 7/5/18.
 */
class CountryPresenter(context: Context) {


    private val generalInfoManager = GeneralInfoManager(context)


    public fun getCountries(onSuccess: (ArrayList<Model.Country>) -> Unit, onError: (Throwable) -> Unit) {
        generalInfoManager.getCountries(onSuccess, onError)
    }

    public fun getCountryById(id: String, onSuccess: (Model.Country) -> Unit, onError: (Throwable) -> Unit) {
        generalInfoManager.getCountryById(id, onSuccess, onError)
    }


}