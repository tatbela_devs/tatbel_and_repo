package com.tatbela.Presenters

import android.content.Context
import com.tatbela.api.Model
import com.tatbela.api.UserInfoManager

/**
 * Created by 3bdoelnaggar on 7/5/18.
 */
class NotificationsPresenter(context: Context) {


    private val userInfoManager = UserInfoManager(context)

    public fun getNotifications(onSuccess: (List<Model.Notification>) -> Unit, onError: (Throwable) -> Unit) {
        userInfoManager.getNotifications(onSuccess, onError)
    }


}