package com.tatbela.Presenters

import android.content.Context
import android.provider.ContactsContract
import com.tatbela.api.LoginManager
import com.tatbela.api.Model
import com.tatbela.view_interfaces.LoginViewInterface

/**
 * Created by 3bdoelnaggar on 7/5/18.
 */
class ForgetPasswordPresenter(context: Context, private val onSuccess: (result: Model.ResponseSwagger) -> Unit, val onError: (Throwable) -> Unit) {


    private val loginManager: LoginManager = LoginManager(context)
    public fun forgetPassword(email:String) {
        loginManager.forgetPassword(email, onSuccess, onError)
    }
}