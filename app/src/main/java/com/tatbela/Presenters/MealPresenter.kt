package com.tatbela.Presenters

import android.content.Context
import com.tatbela.api.MealManager
import com.tatbela.api.Model

/**
 * Created by 3bdoelnaggar on 7/5/18.
 */
class MealPresenter(context: Context) {


    private val mealManager = MealManager(context)

    public fun geteBestMeals(onSuccess: (ArrayList<Model.MealSwagger>) -> Unit, onError: (Throwable) -> Unit) {
        mealManager.getBestMeals(onSuccess, onError)
    }

    public fun getMealById(id: Int, onSuccess: (Model.MealWithReviews) -> Unit, onError: (Throwable) -> Unit) {
        mealManager.getMeal(id, onSuccess, onError)
    }

    public fun getMealsByCategoryId(id: Int, onSuccess: (ArrayList<Model.ChefWithMeals>) -> Unit, onError: (Throwable) -> Unit) {
        mealManager.getMealsByCategoryId(id, onSuccess, onError)
    }

    public fun getCategories(onSuccess: (ArrayList<Model.CategoryWithIcon>) -> Unit, onError: (Throwable) -> Unit) {
        mealManager.getCategories(onSuccess, onError)
    }
}