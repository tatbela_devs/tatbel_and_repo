package com.tatbela.Presenters

import android.content.Context
import com.tatbela.api.LoginManager
import com.tatbela.api.Model

/**
 * Created by 3bdoelnaggar on 7/5/18.
 */
    class CheckUserNamePresenter(context: Context, private val onSuccess: (result: Model.IsUniqueResponse) -> Unit, val onError: (Throwable) -> Unit) {



    private val loginManager: LoginManager = LoginManager(context)
    public fun checkUserName(user:String) {
        loginManager.checkUserName(user, onSuccess, onError)

    }
}