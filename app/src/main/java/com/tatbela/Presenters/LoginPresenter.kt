package com.tatbela.Presenters

import android.content.Context
import com.tatbela.api.LoginManager
import com.tatbela.api.Model
import com.tatbela.view_interfaces.LoginViewInterface

/**
 * Created by 3bdoelnaggar on 7/5/18.
 */
class LoginPresenter(context: Context, private val onLoginSuccess: (result: Model.UserSwagger) -> Unit, val onError: (Throwable) -> Unit) {


    private val loginManager: LoginManager = LoginManager(context)
    public fun login(email: String, password: String) {
        val user = Model.User(null, email, password, null, null, null)
        loginManager.login(user, onLoginSuccess, onError)
    }
}