package com.tatbela.Presenters

import android.content.Context
import com.tatbela.api.LoginManager
import com.tatbela.api.Model
import com.tatbela.view_interfaces.LoginViewInterface

/**
 * Created by 3bdoelnaggar on 7/5/18.
 */
class PinConfirmationPresenter(context: Context, private val onConfirmationSuccess: (result: Model.ResponseSwagger) -> Unit, val onError: (Throwable) -> Unit) {


    val loginManager: LoginManager = LoginManager(context)
    public fun confirmPin(pin:String) {
        loginManager.confirmPin(pin, onConfirmationSuccess, onError)
    }
}