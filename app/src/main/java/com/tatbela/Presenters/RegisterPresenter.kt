package com.tatbela.Presenters

import android.content.Context
import com.tatbela.api.LoginManager
import com.tatbela.api.Model

/**
 * Created by 3bdoelnaggar on 7/5/18.
 */
    class RegisterPresenter(context: Context, private val onRegisterSuccess: (result: Model.ResponseSwagger) -> Unit, val onError: (Throwable) -> Unit) {



    private val loginManager: LoginManager = LoginManager(context)
    public fun register(user:Model.User) {
        loginManager.register(user, onRegisterSuccess, onError)

    }
}