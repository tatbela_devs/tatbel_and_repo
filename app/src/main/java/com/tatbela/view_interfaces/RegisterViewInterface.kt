package com.tatbela.view_interfaces

import com.tatbela.api.Model

/**
 * Created by 3bdoelnaggar on 7/5/18.
 */
interface RegisterViewInterface :BaseViewInterface{
    fun onRegisterSuccess(user: Model.User)
}