package com.tatbela.view_interfaces

/**
 * Created by 3bdoelnaggar on 7/5/18.
 */
interface ForgetPasswordViewInterface :BaseViewInterface{
    fun onForgetPasswordSuccess()
}