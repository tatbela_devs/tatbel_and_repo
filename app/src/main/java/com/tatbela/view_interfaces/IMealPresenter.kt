package com.tatbela.view_interfaces

import com.tatbela.api.Model

/**
 * Created by 3bdoelnaggar on 7/5/18.
 */
interface IMealPresenter {
    fun onGetMealSuccess(meal: Model.Meal)
}