package com.tatbela.view_interfaces

import com.tatbela.api.Model

interface MealViewInterface :BaseViewInterface{
   fun onGetMeal(meal: Model.Meal)

}
