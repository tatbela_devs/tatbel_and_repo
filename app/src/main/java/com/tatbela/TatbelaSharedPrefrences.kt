package com.tatbela

/**
 * Created by Daniel Tadrous on 6/1/2018.
 */

import android.content.Context
import android.preference.PreferenceManager
import com.google.gson.*
import com.tatbela.api.Model

/**
 * Created by daniel.raouf on 12/12/2016.
 */

class TatbelaSharedPrefrences {
    class Configurations {

    }

    companion object {
        fun getConfigurations(context: Context): Configurations {
            val preferences = PreferenceManager.getDefaultSharedPreferences(context)
            val gson = Gson()
            val json = preferences.getString("configs", "")
            val o = gson.fromJson(json, Configurations::class.java)
            return if (o == null) {
                Configurations()
            } else {
                o
            }
        }

        fun setConfigurations(context: Context, configs: Configurations) {
            val preferences = PreferenceManager.getDefaultSharedPreferences(context)
            val prefsEditor = preferences.edit()
            val gson = Gson()
            val json = gson.toJson(configs)
            prefsEditor.putString("configs", json)
            prefsEditor.commit()
        }

        fun getToken(context: Context): String {
            val preferences = PreferenceManager.getDefaultSharedPreferences(context)
            return preferences.getString("token", "")
        }

        fun setToken(context: Context, token: String) {
            val preferences = PreferenceManager.getDefaultSharedPreferences(context)
            val editor = preferences.edit()
            editor.putString("token", token)
            editor.commit()
        }

        fun getUser(context: Context): Model.User? {
            val preferences = PreferenceManager.getDefaultSharedPreferences(context)
            val gson = Gson()
            val json = preferences.getString("user", "")
            val user = gson.fromJson(json, Model.User::class.java)
            return user
        }

        fun setUser(context: Context, user: Model.UserSwagger?) {
            val preferences = PreferenceManager.getDefaultSharedPreferences(context)
            val editor = preferences.edit()
            val gson = Gson()
            val json = gson.toJson(user)
            editor.putString("user", json)
            editor.commit()
        }
    }
}
