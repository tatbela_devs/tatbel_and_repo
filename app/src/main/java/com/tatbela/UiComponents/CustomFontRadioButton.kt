package com.tatbela.UiComponents

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import android.util.Log
import android.widget.RadioButton
import com.tatbela.R

/**
 * Created by Elnaggar on 10/05/2016.
 */
class CustomFontRadioButton : RadioButton {
    constructor(context: Context) : super(context) {
        this.typeface = Typeface.createFromAsset(context.assets, "fonts/thin.ttf")

    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        val sets = intArrayOf(R.attr.custom_font)
        val typedArray = context.obtainStyledAttributes(attrs, sets)
        if (!isInEditMode) {
            if (typedArray.hasValue(R.styleable.CustomFont_custom_font)) {
                val font = typedArray.getText(R.styleable.CustomFont_custom_font)
                if (font.contains("fonts")) {
                    this.typeface = Typeface.createFromAsset(context.assets, font.toString())
                } else {
                    this.typeface = Typeface.createFromAsset(context.assets, "fonts/" + font.toString())

                }
                Log.d("Custom Font", font.toString())

            } else {
               // this.typeface = Typeface.createFromAsset(context.assets, "fonts/thin.ttf")

            }
        }
        typedArray.recycle()
    }
}
