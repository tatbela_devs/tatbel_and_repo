package com.tatbela.UiComponents

import android.content.Context
import android.support.annotation.StyleableRes
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.AttributeSet
import android.view.View
import android.widget.EditText
import android.widget.LinearLayout
import com.mobsandgeeks.saripaar.adapter.ViewDataAdapter
import com.tatbela.R
import kotlinx.android.synthetic.main.edit_text_with_cross.view.*

/**
 * Created by Daniel Tadrous on 6/18/2018.
 */
class EditTextWithIcon(context: Context, attrs: AttributeSet) : LinearLayout(context, attrs), ViewDataAdapter<EditText, String> {
    override fun getData(view: EditText?): String {
        return et.text.toString()
    }

    @StyleableRes
    var icon = 0

    @StyleableRes
    var hint = 1

    @StyleableRes
    var type = 2

    var text: Editable?
        get() {
            return et.text
        }
        set(value) {
            et.text = value
        }

    init {
        init(context, attrs)
    }

    private fun init(context: Context, attrs: AttributeSet) {
        View.inflate(context, R.layout.edit_text_with_cross, this)
        val sets = intArrayOf(R.attr.btn_icon, R.attr.text_hint, R.attr.input_text_type)
        val typedArray = context.obtainStyledAttributes(attrs, sets)
        val btnIcon = typedArray.getDrawable(icon)
        val textHint = typedArray.getText(hint)
        val typeText = context.obtainStyledAttributes(attrs, R.styleable.CustomView)
        initComponents()
        btn.setImageDrawable(btnIcon)
        et.hint = textHint
        btn.visibility = View.INVISIBLE
        et.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if (p0!!.isEmpty()) {
                    btn.visibility = View.INVISIBLE
                } else {
                    btn.visibility = View.VISIBLE
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })

        et.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                lineText.setBackgroundColor(resources.getColor(R.color.colorAccent))
            } else {
                lineText.setBackgroundColor(resources.getColor(R.color.warm_gray))

            }
        }
        btn.setOnClickListener {
            if (typeText.hasValue(R.styleable.CustomView_input_text_type)) {
                if (et.inputType == InputType.TYPE_TEXT_VARIATION_PASSWORD) {
                    et.setRawInputType(InputType.TYPE_TEXT_VARIATION_NORMAL)
                    et.transformationMethod = PasswordTransformationMethod.getInstance()
                    btn.setImageResource(R.drawable.ic_action_eye)
                } else {
                    et.setRawInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD)
                    et.setTransformationMethod(HideReturnsTransformationMethod.getInstance())
                    btn.setImageResource(R.mipmap.noeye)
                }
                et.setSelection(et.getText().length)
            } else {
                et.setText("")
            }
        }

        if (typeText.hasValue(R.styleable.CustomView_input_text_type)) {
            //et.setRawInputType(InputType.TYPE_TEXT_VARIATION_NORMAL)
            val i = typeText.getInt(R.styleable.CustomView_input_text_type, -1)
            when (i) {
                7 -> {
                    et.transformationMethod = PasswordTransformationMethod.getInstance()

                }
                2 -> {
                    et.inputType=InputType.TYPE_CLASS_PHONE
                }
                1->{
                    et.inputType=InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
                }
            }
        }
    }

    private fun initComponents() {
    }

}