package com.tatbela.utils

import android.content.Context
import com.tatbela.api.Token

class Utils {
    fun getToken(context: Context): Token {
        return Token()

    }

    fun getLanguage(context: Context): String? =
            if(context.resources.configuration.locale.language.equals("en")) "English"
            else "Arabic"

    fun getVersion(): CharSequence? {
        return "1.0"
    }
}
