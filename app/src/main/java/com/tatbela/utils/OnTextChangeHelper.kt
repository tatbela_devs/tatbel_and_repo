package com.tatbela.utils

import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.EditText

/**
 * Created by elanaggar on 22/09/17.
 */

class OnTextChangeHelper(onTextChangeListener: OnTextChangeListener, view: View) : TextWatcher {
    private var onTextChangeListener: OnTextChangeListener = onTextChangeListener
    private var view: View= view


    override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

    }

    override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
        onTextChangeListener!!.onTextChanged(view, charSequence, i2)
    }

    override fun afterTextChanged(editable: Editable) {

    }

    fun putListenerON(id1: EditText) {
        id1.addTextChangedListener(this)
    }

    interface OnTextChangeListener {
        fun onTextChanged(view: View, charSequence: CharSequence, count: Int)
    }
}
