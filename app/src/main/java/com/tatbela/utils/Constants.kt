

package com.tatbela.utils
class Constants {

    companion object {

        const val TOKEN = "token"
        const val TYPE_ADDRESS_FROM = 1
        const val TYPE_ADDRESS_TO = 2
       const val KEY_ADDRESS_STRING: String = "addressString"
       const val KEY_ADDRESS_Type: String="addressType"
        const val STATUS_SUCCESS: String = "success"
        const val KEY_COMPLAIN_ID: String = "complain_id"
        const val KEY_CHEF_WITH_RATE: String = "chefWithRateKey"
        const val KEY_MEAL: String = "mealKey"
        const val KEY_CATEGORY: String = "categoryKey"
    }
}

