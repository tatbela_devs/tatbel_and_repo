package com.tatbela.utils

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import android.widget.TextView

/**
 * Created by daniel.raouf on 11/9/2016.
 */
class TatbelaTextView(context: Context, attrs: AttributeSet) : TextView(context, attrs) {

    init {
        if (mTypeface == null)
            mTypeface = Typeface.createFromAsset(context.assets, "fonts/tatbela.ttf")

        typeface = mTypeface
    }

    companion object {
        private var mTypeface: Typeface? = null
    }
}
