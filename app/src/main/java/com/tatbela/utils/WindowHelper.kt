package com.tatbela.utils

import android.os.Build
import android.support.annotation.RequiresApi
import android.view.Window
import android.view.WindowManager

/**
 * Created by 3bdoelnaggar on 6/30/18.
 */
class WindowHelper {
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun changeStatusBarColor(window: Window, color: Int) {
        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

        // finally change the color
        window.statusBarColor = color
    }
}