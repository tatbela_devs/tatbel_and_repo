package com.tatbela.interfaces

/**
 * Created by ma7moud on 10/10/17.
  */

open interface Function<T> {
    fun apply(t: T)
}
