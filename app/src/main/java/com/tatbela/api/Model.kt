package com.tatbela.api

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by Daniel Tadrous on 5/25/2018.
 */
object Model {


        data class Country(
                var id: Int? = 0,
                var name: String? = "",
                var cities: List<City?>? = listOf()
        )
        data class City(
                var id: Int? = 0,
                var name: String? = "",
                var areas: List<Area?>? = listOf()
        )

        data class Area(
                var id: Int? = 0,
                var name: String? = ""
        )

        data class Notification(
                var id: Int? = 0,
                var title: String? = "",
                var body: String? = "",
                var date: String? = ""
        )

    data class ChefWithRate(

            @SerializedName("id")
            @Expose
            var id: Int = 0,
            @SerializedName("name")
            @Expose
            var name: String? = null,
            @SerializedName("address")
            @Expose
            var address: Model.AddressSwagger? = null,
            @SerializedName("image")
            @Expose
            var image: String? = null,
            @SerializedName("meals")
            @Expose
            var meals: List<ChefMeal>? = null,
            @SerializedName("reviews")
            @Expose
            var reviews: List<Model.Review>? = null,
            @SerializedName("rating")
            @Expose
            var rating: Int = 0):Serializable


    data class ChefMeal(

            @SerializedName("id")
            @Expose
            var id: Int = 0,
            @SerializedName("name")
            @Expose
            var name: String? = null,
            @SerializedName("description")
            @Expose
            var description: String? = null,
            @SerializedName("price")
            @Expose
            var price: Double = 0.toDouble(),
            @SerializedName("prepare_time")
            @Expose
            var prepareTime: String? = null,
            @SerializedName("image")
            @Expose
            var image: String? = null,
            @SerializedName("isToday")
            @Expose
            var isIsToday: Boolean = false,
            @SerializedName("chiefName")
            @Expose
            var chiefName: String? = null,
            @SerializedName("categoryName")
            @Expose
            var categoryName: String? = null)

    data class CategoryWithIcon(

            @SerializedName("id")
            @Expose
            var id: Int = 0,
            @SerializedName("name")
            @Expose
            var name: String? = null,
            @SerializedName("icon")
            @Expose
            var icon: String? = null,
            @SerializedName("color")
            @Expose
            var color: String? = null)

    data class ChefWithMeals(

            @SerializedName("id")
            @Expose
            var id: Int = 0,
            @SerializedName("name")
            @Expose
            var name: String? = null,
            @SerializedName("address")
            @Expose
            var address: Model.AddressSwagger? = null,
            @SerializedName("image")
            @Expose
            var image: String? = null,
            @SerializedName("meals")
            @Expose
            var meals: List<Meal>? = null

    ) : Serializable


        data class Meal(

            @SerializedName("id")
            @Expose
            var id: Int = 0,
            @SerializedName("name")
            @Expose
            var name: String? = null,
            @SerializedName("description")
            @Expose
            var description: String? = null,
            @SerializedName("price")
            @Expose
            var price: Double = 0.toDouble(),
            @SerializedName("prepare_time")
            @Expose
            var prepareTime: String? = null,
            @SerializedName("image")
            @Expose
            var image: String? = null) : Serializable


        data class Review(

            @SerializedName("id")
            @Expose
            var id: Int = 0,
            @SerializedName("userImageURL")
            @Expose
            var userImageURL: String? = null,
            @SerializedName("userName")
            @Expose
            var userName: String? = null,
            @SerializedName("date")
            @Expose
            var date: String? = null,
            @SerializedName("description")
            @Expose
            var description: String? = null,
            @SerializedName("rating")
            @Expose
            var rating: Int = 0)


    data class MealWithReviews(

            @SerializedName("id")
            @Expose
            var id: Int = 0,
            @SerializedName("name")
            @Expose
            var name: String? = null,
            @SerializedName("description")
            @Expose
            var description: String? = null,
            @SerializedName("price")
            @Expose
            var price: Double = 0.toDouble(),
            @SerializedName("prepare_time")
            @Expose
            var prepareTime: String? = null,
            @SerializedName("images")
            @Expose
            var images: List<String>? = null,
            @SerializedName("isToday")
            @Expose
            var isIsToday: Boolean = false,
            @SerializedName("chefName")
            @Expose
            var chiefName: String? = null,
            @SerializedName("categoryName")
            @Expose
            var categoryName: String? = null,
            @SerializedName("chef")
            @Expose
            var chief: Model.Chief? = null,
            @SerializedName("category")
            @Expose
            var category: Model.Category? = null,
            @SerializedName("reviews")
            @Expose
            var reviews: List<Review>? = null)


    data class Category(
            @SerializedName("id")
            @Expose
            var id: Int = 0,
            @SerializedName("name")
            @Expose
            var name: String? = null
    )

    data class Chief(

            @SerializedName("id")
            @Expose
            var id: Int = 0,
            @SerializedName("name")
            @Expose
            var name: String? = null,
            @SerializedName("address")
            @Expose
            var address: AddressSwagger? = null,
            @SerializedName("image")
            @Expose
            var image: String? = null,
            @SerializedName("rating")
            @Expose
            var rating: Int = 0
    )


    data class AddressSwagger(

            @SerializedName("lat")
            @Expose
            var lat: Double = 0.toDouble(),
            @SerializedName("lng")
            @Expose
            var lng: Double = 0.toDouble(),
            @SerializedName("description")
            @Expose
            var description: String? = null
    )

    data class MealSwagger(

            @SerializedName("id")
            @Expose
            var id: Int = 0,
            @SerializedName("images")
            @Expose
            var images: List<String>? = null,
            @SerializedName("name")
            @Expose
            var name: String? = null,
            @SerializedName("chef")
            @Expose
            var chief: Chief? = null,
            @SerializedName("category")
            @Expose
            var category: Category? = null

    )

    // data class Meal(val id: Int, val name: String, val description: ArrayList<String>, val price: Double, val rating: Double, val prepare_time: String, val chief: Chef) : Serializable
    data class Response(val status: String, val code: Int) : Serializable

    data class User(val id: Int?, val email: String?, val password: String?, val first_name: String?, val second_name: String?, val phone: String?) : Serializable
    data class Chef(val id: Int?, val name: String, val address: AddressSwagger) : Serializable
    //    data class Address(val description: String, val lat: Float, val lng: Float)
    data class AddressString(val place: String, val howFar: String) : Serializable

    data class UserSwagger(

            var authorization: String? = null,
            var username: String? = null,

            var email: String? = null,

            var phone: String? = null

    )

    data class ResponseSwagger(var status: String? = null)
    data class IsUniqueResponse(var is_unique: Boolean? = null)
    data class AddressSwaggerWithId(

            var id: Int = 0,

            var lat: Double = 0.toDouble(),

            var lng: Double = 0.toDouble(),

            var description: String? = null)

    data class Complain(
            var id: Int = 0,

            var title: String? = null,

            var date: String? = null,

            var description: String? = null,

            var status: Int = 0)

    data class Comment(

            @SerializedName("id")
            @Expose
            var id: Int = 0,
            @SerializedName("userImageURL")
            @Expose
            var userImageURL: String? = null,
            @SerializedName("userName")
            @Expose
            var userName: String? = null,
            @SerializedName("isUserCustomerSupport")
            @Expose
            var isIsUserCustomerSupport: Boolean = false,
            @SerializedName("date")
            @Expose
            var date: String? = null,
            @SerializedName("description")
            @Expose
            var description: String? = null)

    data class ComplainWithComments(

            @SerializedName("id")
            @Expose
            var id: Int = 0,
            @SerializedName("title")
            @Expose
            var title: String? = null,
            @SerializedName("date")
            @Expose
            var date: String? = null,
            @SerializedName("description")
            @Expose
            var description: String? = null,
            @SerializedName("status")
            @Expose
            var status: Int = 0,
            @SerializedName("comments")
            @Expose
            var comments: List<Comment>? = null)


    data class IconTitle(var icon: Int? = null, var title: String? = null)

}


