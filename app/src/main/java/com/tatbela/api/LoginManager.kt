package com.tatbela.api

import android.content.Context
import com.tatbela.interfaces.Function

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by Daniel Tadrous on 6/11/2018.
 */


class LoginManager(context: Context) : BaseApiManager(context) {


    fun register(user: Model.User, function: (result: Model.ResponseSwagger) -> Unit, errorFunction: (Throwable) -> Unit) {
        TatbelaApi.create(context).register(user).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result ->
                            function(result)
                        },
                        { error ->
                            errorFunction(error)
                        }
                )
    }

    fun login(user: Model.User, function: Function<Model.UserSwagger>, errorFunction: Function<Throwable>) {
        TatbelaApi.create(context).signIn(user.email!!, user.password!!).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result ->
                            function.apply(result)
                        }, errorFunction::apply
                )

    }

    fun login(user: Model.User, onSuccess: (Model.UserSwagger) -> Unit, onError: (Throwable) -> Unit) {
        TatbelaApi.create(context).signIn(user.email!!, user.password!!).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result ->
                            onSuccess(result)
                        }, { error ->
                    onError(error)
                }
                )

    }

    fun confirmPin(pin: String, onSuccess: (Model.ResponseSwagger) -> Unit, onError: (Throwable) -> Unit) {
        TatbelaApi.create(context).confirmPin(pin).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result ->
                            onSuccess(result)
                        }, { error ->
                    onError(error)
                }
                )

    }
    fun checkUserName(userName: String, onSuccess: (Model.IsUniqueResponse) -> Unit, onError: (Throwable) -> Unit) {
        TatbelaApi.create(context).checkUserName(userName).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result ->
                            onSuccess(result)
                        }, { error ->
                    onError(error)
                }
                )

    }

    fun forgetPassword(email: String, onSuccess: (Model.ResponseSwagger) -> Unit, onError: (Throwable) -> Unit) {
        TatbelaApi.create(context).forgetPassword(email).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result ->
                            onSuccess(result)
                        },
                        { error ->
                            onError(error)
                        }
                )
    }


}