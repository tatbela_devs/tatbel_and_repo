package com.tatbela.api

import android.content.Context
import okhttp3.MediaType
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit

/**
 * Created by 3bdoelnaggar on 7/5/18.
 */
open class BaseApiManager( val context: Context) {
    private val TIMEOUT_INTERVAL = 60000L

    fun getHttpClient(): OkHttpClient {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        return OkHttpClient.Builder()
                .connectTimeout(TIMEOUT_INTERVAL, TimeUnit.MILLISECONDS)
                .readTimeout(TIMEOUT_INTERVAL, TimeUnit.MILLISECONDS)
                .addInterceptor(logging)
                .addInterceptor { chain ->
                    val newBuilder = chain.request().newBuilder()
                    chain.proceed(newBuilder.build())
                }.build()
    }


    fun createRequestBody(value: String): RequestBody {
        return RequestBody.create(MediaType.parse("text/plain"), value)
    }

    private fun getTempAccssToken(): String? {
        return "1234567879809-0"
    }
}