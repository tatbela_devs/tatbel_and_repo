package com.tatbela.api

import android.content.Context

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


class GeneralInfoManager(context: Context) : BaseApiManager(context) {


    fun getCountries(onSuccess: (ArrayList<Model.Country>) -> Unit, onError: (Throwable) -> Unit) {
        TatbelaApi.create(context).getCountries().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result ->
                            onSuccess(result)
                        }, { error ->
                    onError(error)
                }
                )

    }


    fun getCountryById(id: String, onSuccess: (Model.Country) -> Unit, onError: (Throwable) -> Unit) {
        TatbelaApi.create(context).getCountriesById(id).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result ->
                            onSuccess(result)
                        }, { error ->
                    onError(error)
                }
                )


    }


}