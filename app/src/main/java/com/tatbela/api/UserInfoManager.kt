package com.tatbela.api

import android.content.Context

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by Daniel Tadrous on 6/11/2018.
 */


class UserInfoManager(context: Context) : BaseApiManager(context) {


    fun getAddresses(onSuccess: (ArrayList<Model.AddressSwaggerWithId>) -> Unit, onError: (Throwable) -> Unit) {
        TatbelaApi.create(context).getAddresses().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result ->
                            onSuccess(result)
                        }, { error ->
                    onError(error)
                }
                )

    }

    fun getComplains(onSuccess: (ArrayList<Model.Complain>) -> Unit, onError: (Throwable) -> Unit) {
        TatbelaApi.create(context).getComplains().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result ->
                            onSuccess(result)
                        }, { error ->
                    onError(error)
                }
                )

    }
    fun getNotifications(onSuccess: (List<Model.Notification>) -> Unit, onError: (Throwable) -> Unit) {
        TatbelaApi.create(context).getNotifications().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result ->
                            onSuccess(result)
                        }, { error ->
                    onError(error)
                }
                )

    }

    fun getComplainById(id: String, onSuccess: (Model.ComplainWithComments) -> Unit, onError: (Throwable) -> Unit) {
        TatbelaApi.create(context).getComplainById(id).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result ->
                            onSuccess(result)
                        }, { error ->
                    onError(error)
                }
                )


    }

    fun submitComplain(onSuccess: (Model.ResponseSwagger) -> Unit, onError: (Throwable) -> Unit) {
        TatbelaApi.create(context).submitComplain().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result ->
                            onSuccess(result)
                        }, { error ->
                    onError(error)
                }
                )
    }


}