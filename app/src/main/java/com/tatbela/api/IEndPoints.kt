package com.tatbela.api

import android.content.Context
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

/**
 * Created by Daniel Tadrous on 5/24/2018.
 */

interface TatbelaApi {

    @GET("countries")
    fun getCountries():Observable<ArrayList<Model.Country>>

    @GET("countries/{id}")
    fun getCountriesById(@Path("id") id: String): Observable<Model.Country>

    @GET("notifications")
    fun getNotifications(): Observable<List<Model.Notification>>


    //chef

    @GET("chiefs/{id}")
    fun getChefById(@Path("id") id: String): Observable<Model.ChefWithRate>

    @FormUrlEncoded
    @POST("/cheif/nearby")
    fun getNearByChefs(@Field("lat") lat: Double, @Field("lng") long: Double): Observable<ArrayList<Model.Chef>>

    @GET("ourChiefs/{countryId}/{cityId}/{areaId}")
    fun getOurChefs(@Path("countryId") countryId: Int, @Path("cityId") cityId: Int, @Path("areaId") areaId: Int):Observable<ArrayList<Model.ChefWithRate>>

    @GET("getMeals")
    fun getMeals(): Observable<Model.MealSwagger>

    @FormUrlEncoded
    @POST("users/login")
    fun signIn(@Field("email") email: String, @Field("password") password: String): Observable<Model.UserSwagger>

    @POST("users/register")
    fun register(@Body user: Model.User): Observable<Model.ResponseSwagger>

    @FormUrlEncoded
    @POST("users/forgetPassword")
    fun forgetPassword(@Field("email") email: String): Observable<Model.ResponseSwagger>

    @GET("{id}/meals")
    fun getMealByid(@Path("id") id: Int): Observable<Model>

    @GET("addresses")
    fun getAddresses(): Observable<ArrayList<Model.AddressSwaggerWithId>>

    @FormUrlEncoded
    @POST("users/confirmPin")
    fun confirmPin(@Field("pin") pin: String): Observable<Model.ResponseSwagger>

    @FormUrlEncoded
    @POST("users/checkUsername")
    fun checkUserName(@Field("username") username: String): Observable<Model.IsUniqueResponse>

    //complain
    @GET("complains")
    fun getComplains(): Observable<ArrayList<Model.Complain>>

    @GET("complains/{id}")
    fun getComplainById(@Path("id") id: String): Observable<Model.ComplainWithComments>

    @POST("complains/submit")
    fun submitComplain(): Observable<Model.ResponseSwagger>

    //meals
    @GET("bestMeals")
    fun getBestMeals(): Observable<ArrayList<Model.MealSwagger>>

    @GET("meals/{id}")
    fun getMealById(@Path("id") id: String): Observable<Model.MealWithReviews>

    @GET("category/{id}/meals")
    fun getMealsByCategoryId(@Path("id") categoryId: String): Observable<ArrayList<Model.ChefWithMeals>>

    //categories
    @GET("categories")
    fun getCategories(): Observable<ArrayList<Model.CategoryWithIcon>>

    companion object {

        fun create(context: Context): TatbelaApi {

            val retrofit = Retrofit.Builder()
                    .client(BaseApiManager(context).getHttpClient())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl("http://virtserver.swaggerhub.com/t1184/Tatbela3/1.0.0/")
                    .build()

            return retrofit.create(TatbelaApi::class.java)
        }
    }

}