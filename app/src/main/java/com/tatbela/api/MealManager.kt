package com.tatbela.api

import android.content.Context
import com.tatbela.interfaces.Function
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by 3bdoelnaggar on 7/5/18.
 */
class MealManager(context: Context) : BaseApiManager(context) {
    fun getMeal(id: Int, onResult: (Model.MealWithReviews) -> Unit, onError: (throwable: Throwable) -> Unit) {
        TatbelaApi.create(context).getMealById("" + id).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe({ result -> onResult(result) }, { error ->
                    onError(error)
                })

    }

    fun getBestMeals(onResult: (ArrayList<Model.MealSwagger>) -> Unit, onError: (throwable: Throwable) -> Unit) {
        TatbelaApi.create(context).getBestMeals().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe({ result -> onResult(result) }, { error ->
                    onError(error)
                })

    }


    fun getMealsByCategoryId(id: Int, onResult: (ArrayList<Model.ChefWithMeals>) -> Unit, onError: (throwable: Throwable) -> Unit) {
        TatbelaApi.create(context).getMealsByCategoryId(""+id).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe({ result -> onResult(result) }, { error ->
                    onError(error)
                })

    }

    fun getCategories(onResult: (ArrayList<Model.CategoryWithIcon>) -> Unit, onError: (throwable: Throwable) -> Unit) {
        TatbelaApi.create(context).getCategories().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe({ result -> onResult(result) }, { error ->
                    onError(error)
                })

    }
}