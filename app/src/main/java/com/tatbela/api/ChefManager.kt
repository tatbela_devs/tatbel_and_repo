package com.tatbela.api

import android.content.Context
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by 3bdoelnaggar on 7/5/18.
 */
class ChefManager(context: Context) : BaseApiManager(context) {
    fun getChefById(id: Int, onResult: (Model.ChefWithRate) -> Unit, onError: (throwable: Throwable) -> Unit) {
        TatbelaApi.create(context).getChefById("" + id).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe({ result -> onResult(result) }, { error ->
                    onError(error)
                })

    }

    fun getChefsNearBy(lat:Double,lng:Double,onResult: (ArrayList<Model.Chef>) -> Unit, onError: (throwable: Throwable) -> Unit) {
        TatbelaApi.create(context).getNearByChefs(lat,lng).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe({ result -> onResult(result) }, { error ->
                    onError(error)
                })

    }


    fun getOurChefs(country: Int,city:Int,area:Int, onResult: (ArrayList<Model.ChefWithRate>) -> Unit, onError: (throwable: Throwable) -> Unit) {
        TatbelaApi.create(context).getOurChefs(country,city,area).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe({ result -> onResult(result) }, { error ->
                    onError(error)
                })

    }

}