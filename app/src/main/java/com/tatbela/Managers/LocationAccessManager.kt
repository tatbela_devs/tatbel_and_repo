package com.tatbela.Managers

import android.Manifest
import android.location.Location
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.single.PermissionListener
import android.annotation.SuppressLint
import android.app.Activity
import com.karumi.dexter.Dexter
import com.karumi.dexter.listener.PermissionRequest


/**
 * Created by Daniel Tadrous on 6/12/2018.
 */

interface ILocation {
    fun OnGetLocation(location: Location?)
}

class LocationAccessManager  {

    var  isGpsEnabled:Boolean = false;

    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private var cotext: Activity? = null

    companion object {
        var location:Location? = null
        var locationManager: LocationAccessManager? = null
        fun getInstance(context: Activity): LocationAccessManager {
            if (LocationAccessManager.locationManager == null) locationManager = LocationAccessManager(context)

            return locationManager!!
        }
    }

    constructor(cotext: Activity) {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(cotext)
        this.cotext = cotext
    }
    fun requestLocation() {
        Dexter.withActivity(cotext)
                .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(object : PermissionListener {
                    @SuppressLint("MissingPermission")
                    override fun onPermissionGranted(response: PermissionGrantedResponse) {/* ... */

                        fusedLocationClient.lastLocation
                                .addOnSuccessListener { location: Location? ->
                                    if (location != null){
                                        LocationAccessManager.location = location
                                        (cotext as ILocation).OnGetLocation(location!!)
                                    }
                                }
                    }

                    override fun onPermissionDenied(response: PermissionDeniedResponse) {/* ... */
                    }

                    override fun onPermissionRationaleShouldBeShown(permission: PermissionRequest, token: PermissionToken) {/* ... */
                    }
                }).check()
    }
}