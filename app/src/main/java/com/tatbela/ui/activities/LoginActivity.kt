package com.tatbela.ui.activities

import android.content.Intent
import android.os.Bundle
import android.widget.EditText
import android.widget.Toast
import com.mobsandgeeks.saripaar.ValidationError
import com.mobsandgeeks.saripaar.Validator
import com.mobsandgeeks.saripaar.annotation.Email
import com.mobsandgeeks.saripaar.annotation.NotEmpty
import com.mobsandgeeks.saripaar.annotation.Password
import com.tatbela.Presenters.LoginPresenter

import com.tatbela.R
import com.tatbela.api.Model
import com.tatbela.TatbelaSharedPrefrences
import com.tatbela.UiComponents.EditTextWithIcon
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.edit_text_with_cross.view.*


class LoginActivity : BaseActivity(), Validator.ValidationListener {


    private var presenter: LoginPresenter = LoginPresenter(this,::onLoginSuccess,::onError)


     fun onError(throwable: Throwable) {
        Toast.makeText(this, throwable.message, Toast.LENGTH_SHORT).show()
        TatbelaSharedPrefrences.setUser(this@LoginActivity, null)
        startActivity(Intent(this, MainActivity::class.java))
        finish()

    }

     private fun onLoginSuccess(user: Model.UserSwagger) {
        TatbelaSharedPrefrences.setUser(this@LoginActivity, user)
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }


    override fun onValidationFailed(errors: MutableList<ValidationError>?) {
        for (error in errors!!) {
            val view = error.getView()
            val message = error.getCollatedErrorMessage(this)

            // Display error messages ;)
            if (view is EditText) {
                view.error = message
            } else if (view is EditTextWithIcon) {
                view.et.error = message
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show()
            }
        }
    }

    override fun onValidationSucceeded() {
        val email = emailEt!!.text.toString()
        val pass = passwordEt!!.text.toString()
        //TatbelaSharedPrefrences.setUser(this,Model.User(null,email,pass,null,null,null))
        presenter.login(email, pass)
    }

    @NotEmpty
    @Email
    var emailEt: EditTextWithIcon? = null

    @NotEmpty
    @Password(min = 6, scheme = Password.Scheme.ALPHA_NUMERIC)
    var passwordEt: EditTextWithIcon? = null


    var validator: Validator? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        passwordEt = findViewById(R.id.passwordEt)
        emailEt = findViewById(R.id.emailEt)
        validator = Validator(this)
        validator!!.registerAdapter(EditTextWithIcon::class.java) { view -> view!!.text.toString() }
        validator!!.setValidationListener(this)

        signInBtn.setOnClickListener {
            validator!!.validate()

        }
        signUpBtn!!.setOnClickListener {
            startActivity(Intent(this, RegistrationActivity::class.java))
        }
        forgetPasswordBtn!!.setOnClickListener {
            startActivity(Intent(this, ForgetPasswordActivity::class.java))
            finish()
        }
    }


}
