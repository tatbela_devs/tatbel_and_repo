package com.tatbela.ui.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.mobsandgeeks.saripaar.ValidationError
import com.mobsandgeeks.saripaar.Validator
import com.mobsandgeeks.saripaar.annotation.Email
import com.mobsandgeeks.saripaar.annotation.NotEmpty
import com.tatbela.Presenters.ForgetPasswordPresenter
import com.tatbela.api.LoginManager
import com.tatbela.R
import com.tatbela.api.Model
import com.tatbela.utils.Constants
import com.tatbela.view_interfaces.ForgetPasswordViewInterface

class ForgetPasswordActivity : BaseActivity(), Validator.ValidationListener {
    fun onError(throwable: Throwable) {
        Toast.makeText(this, throwable.message, Toast.LENGTH_SHORT).show()
    }

    private fun onForgetPasswordSuccess(responseSwagger: Model.ResponseSwagger) {
        if (responseSwagger.status == Constants.STATUS_SUCCESS) {
            Toast.makeText(this,"we sent instruction to how to retain password to your mail",Toast.LENGTH_SHORT).show()
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        } else {

        }
    }


    private val presenter = ForgetPasswordPresenter(this, ::onForgetPasswordSuccess, ::onError)

    @NotEmpty
    @Email
    var emailEt: EditText? = null
    var resetBtn: Button? = null

    override fun onValidationFailed(errors: MutableList<ValidationError>?) {
        for (error in errors!!) {
            val view = error.view
            val message = error.getCollatedErrorMessage(this)

            // Display error messages ;)
            if (view is EditText) {
                view.error = message
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show()
            }
        }

    }

    override fun onValidationSucceeded() {
        val email = emailEt!!.text.toString()
        presenter.forgetPassword(email)

    }

    var validator: Validator? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //Activity
        setContentView(R.layout.activity_forget_password)
        emailEt = findViewById(R.id.emailEt)
        resetBtn = findViewById(R.id.resetBtn)

        validator = Validator(this)
        validator!!.setValidationListener(this)

        resetBtn!!.setOnClickListener({
            validator!!.validate()
        })
    }

    override fun onStop() {
        super.onStop()
    }
}
