package com.tatbela.ui.activities

import android.content.Intent
import android.graphics.Typeface
import android.location.Location
import android.os.Bundle
import android.support.design.internal.NavigationMenuView
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.content.ContextCompat
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBar
import android.support.v7.widget.LinearLayoutManager
import android.text.Spannable
import android.text.SpannableString
import android.view.Gravity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.tatbela.Managers.ILocation
import com.tatbela.Managers.LocationAccessManager
import com.tatbela.R
import com.tatbela.TatbelaSharedPrefrences
import com.tatbela.api.Model
import com.tatbela.ui.fragments.*
import com.tatbela.utils.CustomTypefaceSpan
import com.tatbela.utils.PataDividerItemDecoration
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.toolbar.*

class MainActivity : BaseActivity(), NavigationView.OnNavigationItemSelectedListener, ILocation {


    override fun OnGetLocation(location: Location?) {
        (currentFragment as BaseFragment).OnGetLocation(location)
    }


    var currentFragment: Fragment? = Fragment()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initNavigationView()

        setSupportActionBar(toolbar)

        navType = NORMAL

        val actionBar: ActionBar? = supportActionBar
        actionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setHomeAsUpIndicator(R.drawable.menu)
        }
        iv_action.setOnClickListener {
            drawer_layout.openDrawer(Gravity.END)
        }



        changeFragment(HomeFragment())
        //request location
        LocationAccessManager.getInstance(this).requestLocation()
    }

    fun changeToolbarIconAction(icon: Int, onClick: (View) -> Unit) {
        iv_action.setImageResource(icon)
        iv_action.setOnClickListener(onClick)

    }

    private fun initNavigationView() {
        val menuItem = nav_view.getChildAt(0) as NavigationMenuView
        val pataDividerItemDecoration = PataDividerItemDecoration(this, LinearLayoutManager.VERTICAL)
        pataDividerItemDecoration.setDrawable(ContextCompat.getDrawable(this, R.drawable.divider)!!)
        menuItem.addItemDecoration(pataDividerItemDecoration)
        nav_view.setNavigationItemSelectedListener(this)

        val m = nav_view.menu
        for (i in 0 until m.size()) {
            val mi = m.getItem(i)
            //for aapplying a font to subMenu ...
            val subMenu = mi.subMenu
            if (subMenu != null && subMenu.size() > 0) {
                for (j in 0 until subMenu.size()) {
                    val subMenuItem = subMenu.getItem(j)
                    applyFontToMenuItem(subMenuItem)

                }
            }

            applyFontToMenuItem(mi)
            //appplayIcon(mi,nav_view.menu)
        }
    }


    override fun onBackPressed() {
        if (navType == SETTINGS) {
            nav_view.invalidate()
            navType = NORMAL
        }
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            if (supportFragmentManager.backStackEntryCount > 1) {
                currentFragment = supportFragmentManager.fragments[supportFragmentManager.fragments.size - 2]
                if (supportFragmentManager.backStackEntryCount == 2) {
                    currentFragment = HomeFragment()
                }
                changeTitle(currentFragment!!)
                changeIndicatorIcon(currentFragment!!)
                checkCurrentFragmentMenuItem(currentFragment!!)
                //Toast.makeText(this, currentFragment!!.javaClass.simpleName, Toast.LENGTH_SHORT).show()
                super.onBackPressed()

            } else {
                finish()
            }
        }
    }


    private fun getCurrentFragment(): BaseFragment {
        return supportFragmentManager.findFragmentById(R.id.content_frame) as BaseFragment
    }

    private fun checkCurrentFragmentMenuItem(currentFragment: Fragment) {
        when (currentFragment) {
            is HomeFragment -> nav_view.setCheckedItem(R.id.nav_home)
            is MealsFragment -> nav_view.setCheckedItem(R.id.nav_all_meals)
            is OurChefsEditedFragment -> nav_view.setCheckedItem(R.id.nav_our_cheifs)
            is ContactUsFragment -> nav_view.setCheckedItem(R.id.nav_contact_us)
            is AboutUsFragment -> nav_view.setCheckedItem(R.id.nav_about_us)
            is HelpCenterFragment -> nav_view.setCheckedItem(R.id.nav_help_center)
            else -> nav_view.setCheckedItem(0)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
//        if (currentFragment is MealDetailsFragment)
//            menu.add(Menu.NONE, 0, Menu.NONE, "Add To Cart").setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS)
        return true
    }

    private fun reloadPreviousFragment(): Fragment? {
        return if (supportFragmentManager.backStackEntryCount > 2) {
            val backStackEntry = supportFragmentManager
                    .getBackStackEntryAt(supportFragmentManager.backStackEntryCount - 1)
            val tag = backStackEntry.name
            supportFragmentManager.findFragmentByTag(tag)

        } else HomeFragment()

    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            if (currentFragment is HomeFragment) {
                drawer_layout.openDrawer(Gravity.START)
            } else {
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private var navType: Int = 0

    private val SETTINGS: Int = 2
    private val NORMAL = 1

    override fun onNavigationItemSelected(item: MenuItem): Boolean {

        var titleId = R.string.home
        when (item.itemId) {
            R.id.nav_home -> {
                currentFragment = HomeFragment()
            }
            R.id.nav_our_cheifs -> {
                changeFragment(OurChefsEditedFragment())
            }
            R.id.nav_settings -> {

                changeFragment(SettingsFragment.newInstance())
//                nav_view.menu.clear()
//                nav_view.inflateMenu(R.menu.settings_menu)
//                initNavigationView()
//                drawer_layout.openDrawer(Gravity.START)
//                nav_view.itemIconTintList = null
//                navType = SETTINGS
//                return true
                //menuInflater.inflate(R.menu.settings_menu,nav_view.menu)
            }


            R.id.nav_my_orders -> {
                changeFragment(MyOrdersFragment())
            }
            R.id.nav_notifications -> {
                changeFragment(NotificationsFragment())
            }
            R.id.nav_contact_us -> {
                changeFragment(ContactUsFragment())
            }
            R.id.nav_about_us -> {
                changeFragment(AboutUsFragment())
            }

            R.id.nav_help_center -> {
                changeFragment(HelpCenterFragment())
            }
            R.id.nav_log_out -> {
                TatbelaSharedPrefrences.setUser(this, null)
                startActivity(Intent(this, LoginActivity::class.java))
                finish()
            }
            R.id.nav_request -> {
                startActivity(Intent(this, BecomeChefActivity::class.java))
            }
            else -> {
                onSettingMenuItemSelected(item)
            }
        }
        if (item.itemId != R.id.nav_log_out) {
            invalidateOptionsMenu()
        }
        drawer_layout.closeDrawer(GravityCompat.START)

        return true
    }

    private fun onSettingMenuItemSelected(item: MenuItem) {
        when (item.itemId) {
            R.id.nav_my_addresses -> {
                changeFragment(AddressesFragment())
            }
            R.id.nav_my_profile -> {
                changeFragment(MyProfileFragment())
            }
        }
    }


    fun changeFragment(fragment: Fragment) {
        if (fragment::class == currentFragment!!::class) {

        } else {
            currentFragment = fragment
            val fragmentManager: FragmentManager = supportFragmentManager
            fragmentManager.beginTransaction().add(R.id.content_frame, currentFragment).addToBackStack(fragment.javaClass.simpleName).commit()
            changeTitle(currentFragment!!)
            // changeToolbarIconAndAction(currentFragment!!)
            changeIndicatorIcon(currentFragment!!)
            checkCurrentFragmentMenuItem(currentFragment!!)
        }

    }

    private fun changeToolbarIconAndAction(currentFragment: Fragment) {

    }


    private fun changeTitle(currantFragment: Fragment) {
        when (currantFragment) {
            is HomeFragment -> {
                supportActionBar?.setTitle(R.string.home)
            }
            is OurChefsEditedFragment -> {
                supportActionBar?.setTitle(R.string.our_cheifs)
            }
            is MyOrdersFragment -> {
                supportActionBar?.setTitle(R.string.my_orders)
            }
            is NotificationsFragment -> {
                supportActionBar?.setTitle(R.string.notifications)
            }
            is SubmitComplainFragment -> {
                supportActionBar?.setTitle(R.string.submit_complain)
            }
            is AddressesFragment -> {
                supportActionBar?.setTitle(R.string.my_addresses)

            }
            is HelpCenterFragment -> {
                supportActionBar?.setTitle(R.string.customer_support)
            }
            is MyProfileFragment -> {
                supportActionBar?.setTitle(R.string.my_profile)
            }
            is AddAddressFragment -> {
                supportActionBar?.setTitle(getString(R.string.add_new_address))
            }
        }
    }


    private fun changeIndicatorIcon(currantFragment: Fragment) {
        if (currantFragment is HomeFragment) {
            supportActionBar?.setHomeAsUpIndicator(R.drawable.menu)
        } else {
            supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_action_back)
        }
    }


    lateinit var chosenAddress: Model.AddressString
    var addressType: Int = 0

    private fun applyFontToMenuItem(mi: MenuItem) {
        val font = Typeface.createFromAsset(assets, "fonts/thin.ttf")
        if (mi.title != null) {
            val mNewTitle = SpannableString(mi.title)

            mNewTitle.setSpan(CustomTypefaceSpan("", font), 0, mNewTitle.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            //mNewTitle.setSpan( AlignmentSpan.Standard(Layout.Alignment.ALIGN_CENTER), 0, mNewTitle.length, 0)
            mi.title = mNewTitle
        }
    }

    fun hideToolbarIcon() {
        iv_action.visibility = View.GONE
    }

}
