package com.tatbela.ui.activities

import android.content.Intent
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.View
import android.widget.Toast
import com.tatbela.Presenters.PinConfirmationPresenter
import com.tatbela.R
import com.tatbela.api.Model
import com.tatbela.utils.Constants
import com.tatbela.utils.OnTextChangeHelper
import com.tatbela.utils.WindowHelper
import kotlinx.android.synthetic.main.activity_confirmation.*
import kotlinx.android.synthetic.main.shredded_edittext.*


class ConfirmationActivity : AppCompatActivity(), OnTextChangeHelper.OnTextChangeListener {


    private val presenter = PinConfirmationPresenter(this, ::onConfirmationSuccess, ::onError)
    override fun onTextChanged(view: View, charSequence: CharSequence, count: Int) {

        when (view.id) {
            R.id.id1 -> if (count == 1) {
                id2!!.requestFocus()
            }
            R.id.id2 -> if (count == 1) {
                id3!!.requestFocus()
            }
            R.id.id3 -> if (count == 1) {
                id4!!.requestFocus()
            }
            R.id.id4 -> {

            }
        }
    }

    fun onError(error: Throwable) {
        Toast.makeText(this, error.message, Toast.LENGTH_SHORT).show()

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_confirmation)
        initView()
        btn_confirm.setOnClickListener {
            val code = id1.text.toString() + id2.text.toString() + id3.text.toString() + id4.text.toString()
            if (code.length < 4) {
                Toast.makeText(this, "Please write The Code You have Or Ask For it Again"
                        , Toast.LENGTH_SHORT).show()
            } else {
                presenter.confirmPin(code)
                Toast.makeText(this, "Confirming Your Code :  $code", Toast.LENGTH_SHORT).show()
            }
        }


    }

    private fun initView() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            WindowHelper().changeStatusBarColor(window, ContextCompat.getColor(this, R.color.maizeDark))
        }
        id1.addTextChangedListener(OnTextChangeHelper(this, id1))
        id2.addTextChangedListener(OnTextChangeHelper(this, id2))
        id3.addTextChangedListener(OnTextChangeHelper(this, id3))
        id4.addTextChangedListener(OnTextChangeHelper(this, id4))
        backBtnImg.setOnClickListener {
            this.onBackPressed()
        }

    }

    private fun onConfirmationSuccess(responseSwagger: Model.ResponseSwagger) {
        if (responseSwagger.status == Constants.STATUS_SUCCESS) {
            startActivity(Intent(this, RegistrationDoneActivity::class.java))


        }

    }


}


