package com.tatbela.ui.activities

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import com.tatbela.R
import com.tatbela.TatbelaSharedPrefrences

/**
 * Created by Daniel Tadrous on 5/28/2018.
 */
class SplashActivity : BaseActivity() {

    private val SPLASH_DELAY: Long = 5000

    private val mHandler = Handler()
    private val mLauncher = Launcher()

    override fun onStart() {
        super.onStart()
        mHandler.postDelayed(mLauncher, SPLASH_DELAY)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
    }

    override fun onStop() {
        mHandler.removeCallbacks(mLauncher)
        super.onStop()
    }

    private fun launch() {
        if (!isFinishing) {
            var user = TatbelaSharedPrefrences.getUser(this@SplashActivity)
            if (user != null) {
                startActivity(Intent(this, MainActivity::class.java))
            } else {
                startActivity(Intent(this, LoginActivity::class.java))
            }
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
            finish()
        }
    }

    private inner class Launcher : Runnable {
        override fun run() {
            launch()
        }
    }
}