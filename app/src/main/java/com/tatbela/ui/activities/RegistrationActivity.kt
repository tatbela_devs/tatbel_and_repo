package com.tatbela.ui.activities

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.widget.EditText
import android.widget.Toast
import com.mobsandgeeks.saripaar.ValidationError
import com.mobsandgeeks.saripaar.Validator
import com.mobsandgeeks.saripaar.annotation.*
import com.tatbela.Presenters.CheckUserNamePresenter
import com.tatbela.Presenters.RegisterPresenter
import com.tatbela.R
import com.tatbela.api.Model
import com.tatbela.UiComponents.EditTextWithIcon
import com.tatbela.utils.Constants
import com.tatbela.utils.WindowHelper
import kotlinx.android.synthetic.main.activity_registeration.*
import kotlinx.android.synthetic.main.edit_text_with_cross.view.*

class RegistrationActivity : BaseActivity(), Validator.ValidationListener {

    var presenter: RegisterPresenter = RegisterPresenter(this, ::onRegisterSuccess,::onError)
    var checkUserNamePresenter: CheckUserNamePresenter = CheckUserNamePresenter(this, ::onUserNameChecked,::onError)

    private fun onUserNameChecked(isUniqueResponse: @ParameterName(name = "result") Model.IsUniqueResponse) {
        if(isUniqueResponse.is_unique!=null&& isUniqueResponse.is_unique!!){
            val email = emailEt!!.text.toString()
            val pass = passwordEt!!.text.toString()
            val phoneNumber = phoneEt!!.text.toString()
            val userName = userNameEt!!.text.toString()
            val user = Model.User(null, email, pass, userName, null, phoneNumber)
            presenter.register(user)

        }
    }

    fun onError(throwable: Throwable) {
        Toast.makeText(this, throwable.message, Toast.LENGTH_SHORT).show()
    }

     private fun onRegisterSuccess(result: Model.ResponseSwagger) {
         if(result.status==Constants.STATUS_SUCCESS) {
             startActivity(Intent(this, ConfirmationActivity::class.java))
             finish()
         }
    }

    override fun onValidationFailed(errors: MutableList<ValidationError>?) {
        for (error in errors!!) {
            val view = error.view
            val message = error.getCollatedErrorMessage(this)

            // Display error messages ;)
            when (view) {
                is EditText -> view.error = message
                is EditTextWithIcon -> view.et.error = message
                else -> Toast.makeText(this, message, Toast.LENGTH_LONG).show()
            }
        }
    }

    override fun onValidationSucceeded() {

        val userName = userNameEt!!.text.toString()

        checkUserNamePresenter.checkUserName(userName)
    }

    @NotEmpty
    @Email
    var emailEt: EditTextWithIcon? = null

    @NotEmpty
    @Length(min = 3)
    var userNameEt: EditTextWithIcon? = null

    @NotEmpty
    @Length(min = 11)
    var phoneEt: EditTextWithIcon? = null

    @NotEmpty
    @Password(min = 6, scheme = Password.Scheme.ALPHA_NUMERIC)
    var passwordEt: EditTextWithIcon? = null


    var validator: Validator? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registeration)
        userNameEt=findViewById(R.id.userNameEt)
        phoneEt=findViewById(R.id.phoneEt)
        passwordEt=findViewById(R.id.passwordEt)
        emailEt=findViewById(R.id.emailEt)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            WindowHelper().changeStatusBarColor(window, ContextCompat.getColor(this, R.color.sapGreen))
        }
        validator = Validator(this)
        validator!!.registerAdapter(EditTextWithIcon::class.java) { view -> view!!.text.toString() }
        validator!!.setValidationListener(this);

        signUpBtn!!.setOnClickListener {
            validator!!.validate()

        }

        loginBtn!!.setOnClickListener {
            startActivity(Intent(this, LoginActivity::class.java))
        }

        backBtnImg.setOnClickListener {
            this.onBackPressed()
        }
    }
}
