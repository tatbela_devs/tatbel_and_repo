package com.tatbela.ui.activities

import android.support.v7.app.AppCompatActivity

/**
 * Created by Daniel Tadrous on 5/26/2018.
 */
open class BaseActivity : AppCompatActivity() {

}