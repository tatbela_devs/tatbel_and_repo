package com.tatbela.ui.activities

import android.os.Build
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import com.tatbela.R
import com.tatbela.utils.WindowHelper

class BecomChefBankAccountActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_becom_chef_bank_account)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            WindowHelper().changeStatusBarColor(window, ContextCompat.getColor(this, R.color.sapGreen))
        }
    }
}
