package com.tatbela.ui.activities

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import com.tatbela.R
import com.tatbela.utils.WindowHelper
import kotlinx.android.synthetic.main.activity_registration_done.*

/**
 * Created by 3bdoelnaggar on 7/1/18.
 */
class RegistrationDoneActivity: AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration_done)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            WindowHelper().changeStatusBarColor(window, ContextCompat.getColor(this,R.color.rustyOrange))
        }
        btn_make_first_order.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
        }
    }

}