package com.tatbela.ui.adapters

import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.tatbela.R
import kotlinx.android.synthetic.main.address_edit_list_item.view.*

/**
 * Created by 3bdoelnaggar on 7/9/18.
 */
open class AddressAdapter(val list_item: Int, val onItemClicked: (Int) -> Unit) : RecyclerView.Adapter<AddressAdapter.ThisViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ThisViewHolder {
        return ThisViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.address_edit_list_item, parent, false))
    }

    override fun getItemCount(): Int {
        return 10
    }

    override fun onBindViewHolder(holder: ThisViewHolder, position: Int) {
        holder.itemView.setOnClickListener {
            onItemClicked(position)
        }
        holder.itemView.setOnLongClickListener {
//            holder.mContent.translationX = -(holder.itemView.context.resources.getDimension(R.dimen.translatex_address_item) / holder.itemView.context.resources.displayMetrics.density)
            holder.mContent.translationX = -(holder.mContent.width/4).toFloat()

            holder.mEdit.visibility = View.VISIBLE
            true
        }
    }

    inner class ThisViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val mEdit: LinearLayout = this.view.llEdit
        val mContent: ConstraintLayout = view.cl_content
    }

}