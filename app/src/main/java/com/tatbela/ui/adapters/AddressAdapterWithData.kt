package com.tatbela.ui.adapters

import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.tatbela.R
import com.tatbela.api.Model
import kotlinx.android.synthetic.main.address_edit_list_item.view.*

/**
 * Created by 3bdoelnaggar on 7/9/18.
 */
open class AddressAdapterWithData(private val items: ArrayList<Model.AddressSwaggerWithId>, private val onItemClicked: (Int) -> Unit) : RecyclerView.Adapter<AddressAdapterWithData.ThisViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ThisViewHolder {
        return ThisViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.address_edit_list_item, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ThisViewHolder, position: Int) {
        holder.itemView.setOnClickListener {
            onItemClicked(position)
        }
        holder.address.text = items[position].description
        holder.addressNum.text = "Address #" + items[position].id.toString()

        holder.itemView.setOnLongClickListener {
            //            holder.mContent.translationX = -(holder.itemView.context.resources.getDimension(R.dimen.translatex_address_item) / holder.itemView.context.resources.displayMetrics.density)
            holder.mContent.translationX = -(holder.mContent.width / 2).toFloat()
            holder.mDelete.visibility = View.VISIBLE
            holder.mEdit.visibility = View.VISIBLE
            true
        }
    }

    inner class ThisViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val mEdit: LinearLayout = this.view.llEdit
        val mDelete: LinearLayout = this.view.llDelete
        val mContent: ConstraintLayout = view.cl_content
        val address: TextView = view.tv_address
        val addressNum: TextView = view.tv_AddressNum
    }

}