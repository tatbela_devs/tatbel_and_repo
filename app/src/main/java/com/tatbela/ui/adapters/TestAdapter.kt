package com.tatbela.ui.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

/**
 * Created by 3bdoelnaggar on 7/9/18.
 */
open class TestAdapter(val list_item: Int, val onItemClicked: (Int) -> Unit) : RecyclerView.Adapter<TestAdapter.ThisViewHolder>() {
    private var count: Int = -1

    constructor(count: Int, list_item: Int, onItemClicked: (Int) -> Unit) : this(list_item, onItemClicked) {
        this.count = count
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ThisViewHolder {
        return ThisViewHolder(LayoutInflater.from(parent.context).inflate(list_item, parent, false))
    }

    override fun getItemCount(): Int {
        return if (count == -1) 10
        else count
    }

    override fun onBindViewHolder(holder: ThisViewHolder, position: Int) {
        holder.itemView.setOnClickListener {
            onItemClicked(position)
        }
    }

    class ThisViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView)

}