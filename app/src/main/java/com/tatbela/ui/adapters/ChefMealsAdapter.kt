package com.tatbela.ui.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.tatbela.R
import com.tatbela.api.Model
import kotlinx.android.synthetic.main.meal_chef_list_item.view.*

/**
 * Created by 3bdoelnaggar on 7/9/18.
 */
open class ChefMealsAdapter(private val items: List<Model.ChefMeal>?, val onItemClicked: (Int) -> Unit) : RecyclerView.Adapter<ChefMealsAdapter.ThisViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ThisViewHolder {
        return ThisViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.meal_chef_list_item, parent, false))
    }

    override fun getItemCount(): Int {
        return items?.size ?: 0
    }

    override fun onBindViewHolder(holder: ThisViewHolder, position: Int) {
        val item = items!![position]
        holder.price.text = item.price.toString()
        Glide.with(holder.itemView.context).load(item.image).into(holder.image)
        holder.mealChef.text = item.chiefName
        holder.mealTime.text = item.prepareTime
        holder.name.text = item.name
        holder.mealType.text = item.categoryName
        holder.itemView.setOnClickListener {
            onItemClicked(position)
        }
    }

    class ThisViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val price: TextView = itemView.tv_mealPrice
        val image: ImageView = itemView.iv_mealPic
        val name: TextView = itemView.tv_mealName
        val mealType: TextView = itemView.tv_mealType
        val mealChef: TextView = itemView.tv_chefName
        val mealTime: TextView = itemView.tv_time
    }

}