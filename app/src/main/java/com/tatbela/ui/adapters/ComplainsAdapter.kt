package com.tatbela.ui.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.tatbela.R
import com.tatbela.api.Model
import kotlinx.android.synthetic.main.complain_list_item.view.*

/**
 * Created by 3bdoelnaggar on 7/9/18.
 */
open class ComplainsAdapter(private val items:ArrayList<Model.Complain>, private val onItemClicked:(Int)->Unit) : RecyclerView.Adapter<ComplainsAdapter.ThisViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ThisViewHolder {
        return ThisViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.complain_list_item, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ThisViewHolder, position: Int) {
        holder.itemView.setOnClickListener {
            onItemClicked(position)
        }

        holder.complainTitle.text=items[position].title
        holder.complainDate.text=items[position].date

        holder.complainContent.text=items[position].description
    }

    class ThisViewHolder(view: View) : RecyclerView.ViewHolder(view){
        val complainDate: TextView =view.tv_complainDate
        val complainContent: TextView =view.tv_complainMessage
        val complainTitle: TextView =view.tv_complainTitle
    }

}