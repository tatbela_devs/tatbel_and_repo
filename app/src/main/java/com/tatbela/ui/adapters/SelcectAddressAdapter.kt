package com.tatbela.ui.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.CompoundButton
import com.tatbela.R

/**
 * Created by 3bdoelnaggar on 7/9/18.
 */
open class SelcectAddressAdapter(val onItemClicked:(Int)->Unit) : RecyclerView.Adapter<SelcectAddressAdapter.ThisViewHolder>() {
   private var isBinding: Boolean = false

     var chosenPosition:Int= -1



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ThisViewHolder {
        return ThisViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.selectable_address_list_item, parent, false))
    }

    override fun getItemCount(): Int {
        return 5
    }

    override fun onBindViewHolder(holder: ThisViewHolder, position: Int) {
        holder.setIsRecyclable(false)
        isBinding=true
        holder.checkBox.isChecked = chosenPosition==position

        holder.itemView.setOnClickListener {
         //   onItemClicked(position)
            chosenPosition=position
            notifyDataSetChanged()
            onItemClicked(position)
        }
        isBinding=false
    }

  class ThisViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView), CompoundButton.OnCheckedChangeListener {
       var checkBox: CheckBox=itemView!!.findViewById(R.id.cb_selected)

       internal var isBinding:Boolean=false
       internal var chosenPosition=-1

      override fun onCheckedChanged(p0: CompoundButton?, p1: Boolean) {
           if(!isBinding){
                  chosenPosition= adapterPosition
           }

      }

       init {
           checkBox.setOnCheckedChangeListener(this)
           checkBox.isClickable=false
       }
   }

}