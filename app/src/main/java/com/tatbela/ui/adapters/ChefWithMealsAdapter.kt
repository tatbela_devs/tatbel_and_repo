package com.tatbela.ui.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.tatbela.R
import com.tatbela.api.Model
import com.tatbela.ui.fragments.MealsFragment
import kotlinx.android.synthetic.main.chef_list_item.view.*
import kotlinx.android.synthetic.main.meal_chef_list_item.view.*

/**
 * Created by 3bdoelnaggar on 7/9/18.
 */
open class ChefWithMealsAdapter(private val list: ArrayList<Model.ChefWithMeals>, val onItemClicked: MealsFragment) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var items: ArrayList<Any> = ArrayList()

    init {
        for (x in list) {
            items.add(x)
        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == R.layout.chef_list_item) {
            return ChefViewHolder(LayoutInflater.from(parent.context).inflate(viewType, parent, false))

        } else {
            return MealViewHolder(LayoutInflater.from(parent.context).inflate(viewType, parent, false))

        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (items[position] is Model.ChefWithMeals) {
            R.layout.chef_list_item
        } else {
            R.layout.meal_chef_list_item
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    private var chosenPositions: ArrayList<Int> = java.util.ArrayList()

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = items[position]
        holder.itemView.setOnClickListener {
            if (holder.itemViewType == R.layout.chef_list_item) {
                if (chosenPositions.contains(position)) {
                    shrink(position, item as Model.ChefWithMeals)
                    chosenPositions.remove(position)
                } else {
                    expand(position, item as Model.ChefWithMeals)
                    chosenPositions.add(position)

                }
            } else {
                onItemClicked(item as Model.Meal)
            }
        }
        if (holder is ChefViewHolder) {
            val chefWithMeals = item as Model.ChefWithMeals
            holder.name.text = (chefWithMeals).name
            holder.address.text = chefWithMeals.address!!.description
            Glide.with(holder.itemView.context).load(chefWithMeals.image).into(holder.image)

        } else {
            holder as MealViewHolder
            val meal = item as Model.Meal
            holder.price.text = meal.price.toString()
            Glide.with(holder.itemView.context).load(meal.image).into(holder.image)

            //holder.mealChef.text = meal.chiefName
            holder.mealTime.text = meal.prepareTime
            holder.name.text = meal.name
            holder.description.text = meal.description
            //holder.mealType.text = item.categoryName

        }
    }

    private fun shrink(position: Int, item: Model.ChefWithMeals) {

        for (i in item.meals!!.size downTo 1) {
            items.removeAt(position + i)
        }
        notifyItemRangeRemoved(position, item.meals!!.size)
    }

    // private fun expand(position: Int, item: Model.ChefWithMeals) {
//        for ((index,value) in item.meals!!) {
//            items.add(position + index , value!!)
//        }
//        notifyItemRangeChanged(position, item.meals!!.size)
//
//    }

    private fun expand(position: Int, item: Model.ChefWithMeals) {
        for (i in 0 until item.meals!!.size) {
            items.add(position + i + 1, item.meals!![i])
        }
        notifyItemRangeChanged(position, item.meals!!.size)

    }

    class ThisViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
    class ChefViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name: TextView = itemView.tv_chef_name
        val address: TextView = itemView.tv_chef_address
        val image: ImageView = itemView.iv_chef_icon

    }

    class MealViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val price: TextView = itemView.tv_mealPrice
        val image: ImageView = itemView.iv_mealPic
        val name: TextView = itemView.tv_mealName
        val mealType: TextView = itemView.tv_mealType
        val mealChef: TextView = itemView.tv_chefName
        val mealTime: TextView = itemView.tv_time
        val description: TextView = itemView.tv_mealDescription

    }

}