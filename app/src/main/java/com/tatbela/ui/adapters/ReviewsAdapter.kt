package com.tatbela.ui.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import com.bumptech.glide.Glide
import com.tatbela.R
import com.tatbela.api.Model
import kotlinx.android.synthetic.main.chef_review_list_item.view.*

/**
 * Created by 3bdoelnaggar on 7/9/18.
 */
open class ReviewsAdapter(val items: ArrayList<Model.Review>, val onItemClicked: (Int) -> Unit) : RecyclerView.Adapter<ReviewsAdapter.ThisViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ThisViewHolder {
        return ThisViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.chef_review_list_item, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ThisViewHolder, position: Int) {
        holder.itemView.setOnClickListener {
            onItemClicked(position)
        }

        val item = items[position]
        holder.name.text = item.userName
        holder.content.text = item.description
        holder.date.text = item.date
        holder.rate.rating = item.rating.toFloat()
        Glide.with(holder.itemView.context).load(item.userImageURL).into(holder.icon)

    }

    class ThisViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name: TextView = itemView.tv_reviewer_name
        val content: TextView = itemView.tv_review
        val date: TextView = itemView.tv_review_date
        val icon: ImageView = itemView.iv_reviewer_icon
        val rate: RatingBar = itemView.rb_chef_rate

    }

}