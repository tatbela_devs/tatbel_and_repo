package com.tatbela.ui.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import com.bumptech.glide.Glide
import com.tatbela.R
import com.tatbela.api.Model
import kotlinx.android.synthetic.main.chef_rate_list_item.view.*

/**
 * Created by 3bdoelnaggar on 7/9/18.
 */
open class ChefsWithRateAdapter(private val items: ArrayList<Model.ChefWithRate>, val onItemClicked: (Int) -> Unit) : RecyclerView.Adapter<ChefsWithRateAdapter.ThisViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ThisViewHolder {
        return ThisViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.chef_rate_list_item, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ThisViewHolder, position: Int) {
        val item = items[position]

        holder.name.text = item.name
        holder.address.text = item.address!!.description
        Glide.with(holder.itemView.context).load(item.image).into(holder.image)
        holder.rate.rating= item.rating.toFloat()




        holder.itemView.setOnClickListener {
            onItemClicked(position)
        }
    }

    class ThisViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name: TextView = itemView.tv_chef_name
        val address: TextView = itemView.tv_chef_address
        val image: ImageView = itemView.iv_chef_icon
        val rate:RatingBar= itemView.rb_chef_rate
    }

}