package com.tatbela.ui.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.tatbela.R
import com.tatbela.api.Model
import kotlinx.android.synthetic.main.notification_list_item.view.*

/**
 * Created by 3bdoelnaggar on 7/9/18.
 */
open class NotificationAdapter(private val items: ArrayList<Model.Notification>, val onItemClicked: (Int) -> Unit) : RecyclerView.Adapter<NotificationAdapter.ThisViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ThisViewHolder {
        return ThisViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.notification_list_item, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ThisViewHolder, position: Int) {
        holder.title.text = items[position].title
        holder.time.text = items[position].date
        holder.message.text = items[position].body


        holder.itemView.setOnClickListener {
            onItemClicked(position)
        }
    }

    class ThisViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title: TextView = itemView.tv_notificationTitle
        val time: TextView = itemView.tv_notificationDate
        val message: TextView = itemView.tv_notificationMessage

    }

}