package com.tatbela.ui.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.tatbela.R
import com.tatbela.api.Model
import kotlinx.android.synthetic.main.best_meal_list_item.view.*

/**
 * Created by 3bdoelnaggar on 7/9/18.
 */
open class BestMealAdapter(val items: ArrayList<Model.MealSwagger>, val onItemClicked: (Int) -> Unit) : RecyclerView.Adapter<BestMealAdapter.ThisViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ThisViewHolder {
        return ThisViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.best_meal_list_item, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ThisViewHolder, position: Int) {
        holder.itemView.setOnClickListener {
            onItemClicked(position)
        }
        val item = items[position]
        holder.chefName.text = item.chief!!.name
        Glide.with(holder.itemView.context).load(item.images!![0]).into(holder.image)
        holder.type.text=item.category!!.name
        holder.mealName.text=item.name
    }

    class ThisViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val mealName:TextView=itemView.tv_mealName
        val image: ImageView = itemView.iv_mealImage
        val type: TextView = itemView.tv_mealType
        val chefName: TextView = itemView.tv_chefName
    }

}