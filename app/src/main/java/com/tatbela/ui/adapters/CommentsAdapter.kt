package com.tatbela.ui.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.tatbela.R
import com.tatbela.api.Model
import kotlinx.android.synthetic.main.comment_list_item.view.*

/**
 * Created by 3bdoelnaggar on 7/9/18.
 */
open class CommentsAdapter(private val comments: List<Model.Comment>) : RecyclerView.Adapter<CommentsAdapter.ThisViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ThisViewHolder {
        return ThisViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.comment_list_item, parent, false))
    }

    override fun getItemCount(): Int {
        return comments.size
    }

    override fun onBindViewHolder(holder: ThisViewHolder, position: Int) {
       val comment= comments[position]
        holder.comment.text=comment.description
        holder.date.text=comment.date
        holder.name.text=comment.userName

    }

    class ThisViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        val comment:TextView= itemView.tv_comment
        val date :TextView= itemView.tv_comment_date
        val name:TextView= itemView.tv_commenter_name
    }

}