package com.tatbela.ui.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.tatbela.R
import com.tatbela.api.Model
import com.tatbela.utils.Utils
import kotlinx.android.synthetic.main.menu_list_item.view.*

class SettingsMenuAdapter(private val iconTitleItems: ArrayList<Model.IconTitle>, private val onItemSelected: (Int) -> Unit) : RecyclerView.Adapter<SettingsMenuAdapter.ThisViewHolder>() {

    private var selectedPosition = RecyclerView.NO_POSITION

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ThisViewHolder {

        return ThisViewHolder(LayoutInflater.from(p0.context).inflate(R.layout.menu_list_item, p0, false))
    }

    override fun getItemCount(): Int {
        return iconTitleItems.size
    }

    override fun onBindViewHolder(p0: ThisViewHolder, p1: Int) {
        val item = iconTitleItems[p1]
        p0.icon.setImageResource(item.icon!!)
        p0.title.text = item.title
        p0.itemView.setOnClickListener {
            onItemSelected(item.icon!!)
        }
        if(item.icon==R.drawable.ic_menu_language) {
            p0.extra.visibility = View.VISIBLE
            p0.extra.text = Utils().getLanguage(p0.itemView.context)
        }
        if(item.icon==R.drawable.ic_menu_app_version){
            p0.extra.visibility = View.VISIBLE
            p0.extra.text = Utils().getVersion()
        }

    }

    class ThisViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val icon: ImageView = itemView.icon
        val title: TextView = itemView.title
        val extra:TextView=itemView.tv_extraDetail
    }
}