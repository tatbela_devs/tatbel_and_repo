package com.tatbela.ui.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tatbela.R

/**
 * Created by 3bdoelnaggar on 7/9/18.
 */
open class ExpandeableTestAdapter (val onItemClicked:(Int)->Unit): RecyclerView.Adapter<ExpandeableTestAdapter.ThisViewHolder>() {
    var items: ArrayList<Int> = ArrayList()

    init {
        items.add(R.layout.chef_list_item)
        items.add(R.layout.chef_list_item)
        items.add(R.layout.chef_list_item)
        items.add(R.layout.chef_list_item)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ThisViewHolder {
        return ThisViewHolder(LayoutInflater.from(parent.context).inflate(viewType, parent, false))
    }

    override fun getItemViewType(position: Int): Int {
        return items[position]
    }

    override fun getItemCount(): Int {
        return items.size
    }

    private var chosenPositions:ArrayList<Int> = java.util.ArrayList()

    override fun onBindViewHolder(holder: ThisViewHolder, position: Int) {
        holder.itemView.setOnClickListener {
         if(holder.itemViewType == R.layout.chef_list_item) {
            if (chosenPositions.contains(position)) {
                shrink(position)
                chosenPositions.remove(position)
            } else {
                expand(position)
                chosenPositions.add(position)

            }
        }else{
             onItemClicked(position)
         }
        }
    }

    private fun shrink(position: Int) {

        for (i in 10 downTo 1){
            items.removeAt(position+i)
        }
        notifyItemRangeRemoved(position,10)
    }

    fun expand(position: Int) {
        for (i in 0..9) {
            items.add(position + i + 1, R.layout.meal_chef_list_item)
        }
        notifyItemRangeChanged(position, 10)

    }

    class ThisViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView)

}