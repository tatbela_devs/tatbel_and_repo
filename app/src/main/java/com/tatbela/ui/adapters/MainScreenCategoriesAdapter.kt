package com.tatbela.ui.adapters

import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.tatbela.R
import com.tatbela.api.Model
import kotlinx.android.synthetic.main.category_list_item.view.*

/**
 * Created by 3bdoelnaggar on 7/9/18.
 */
open class MainScreenCategoriesAdapter(val items: ArrayList<Model.CategoryWithIcon>, private val onItemClicked: (Int) -> Unit) : RecyclerView.Adapter<MainScreenCategoriesAdapter.ThisViewHolder>() {



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ThisViewHolder {
        return ThisViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.category_list_item, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ThisViewHolder, position: Int) {
        val item = items[position]


        holder.itemView.setOnClickListener {

            onItemClicked(position)
        }

        Glide.with(holder.itemView.context).load(item.icon).into(holder.image)
        holder.name.text = item.name
        holder.name.setTextColor(Color.parseColor(item.color))
    }

    class ThisViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val image: ImageView = itemView.iv_category
        val name: TextView = itemView.tv_categoryName



    }

}