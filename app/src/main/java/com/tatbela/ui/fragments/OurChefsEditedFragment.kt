package com.tatbela.ui.fragments

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.tatbela.Presenters.ChefPresenter
import com.tatbela.R
import com.tatbela.api.Model
import com.tatbela.ui.adapters.ChefsWithRateAdapter
import com.tatbela.utils.Constants
import kotlinx.android.synthetic.main.fragment_our_cheifs.*

class OurChefsEditedFragment : BaseFragment(), (Int) -> Unit {




    override fun invoke(p1: Int) {

        val fragment = ChefFragment()
       val bundle=Bundle()
        bundle.putSerializable(Constants.KEY_CHEF_WITH_RATE,chefsWithRate[p1])
        fragment.arguments=bundle

        changeFragment(fragment)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        // Inflate the layout for this fragment
        return inflater?.inflate(R.layout.fragment_our_cheifs_edited,
                container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val presenter = ChefPresenter(context!!)
        presenter.getOurChefs(1, 2, 3, ::onGetOurChefsSuccess, ::onGetOurChefsError)

        recyclerView_chefList.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)

    }






    private fun onGetOurChefsError(throwable: Throwable) {
        Toast.makeText(context,throwable.message,Toast.LENGTH_SHORT).show()

    }

    private lateinit var chefsWithRate: ArrayList<Model.ChefWithRate>

    private fun onGetOurChefsSuccess(arrayList: ArrayList<Model.ChefWithRate>) {
        chefsWithRate=arrayList
        recyclerView_chefList.adapter = ChefsWithRateAdapter(arrayList, this)


    }
}