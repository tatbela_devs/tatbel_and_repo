package com.tatbela.ui.fragments

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.tatbela.Presenters.ComplainPresenter
import com.tatbela.R
import com.tatbela.api.Model
import com.tatbela.ui.adapters.ComplainsAdapter
import com.tatbela.utils.Constants
import kotlinx.android.synthetic.main.fragment_help_center.*

class HelpCenterFragment : BaseFragment() {
     fun onItemClicked(position: Int) {

        val fragment = MyComplainFragment()
        val bundle = Bundle()
        bundle.putInt(Constants.KEY_COMPLAIN_ID,position)
        fragment.arguments= bundle

        changeFragment(fragment)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return LayoutInflater.from(context).inflate(R.layout.fragment_help_center, container, false)
    }

    fun onError(errThrowable: Throwable) {
        Toast.makeText(context, errThrowable.message, Toast.LENGTH_SHORT).show()

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val presenter = ComplainPresenter(context!!)
        presenter.getComplains(::onGetComplains, ::onError)
        super.onViewCreated(view, savedInstanceState)
        recyclerView_complains.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)

        changeToolbarIconAction(R.drawable.ic_state_on_delivery, ::onAddComplainClicked)
    }

    private fun onGetComplains(arrayList: ArrayList<Model.Complain>) {
        recyclerView_complains.adapter = ComplainsAdapter(arrayList, ::onItemClicked)
    }

    private fun onAddComplainClicked(view: View) {
        changeFragment(SubmitComplainFragment())
    }


}

