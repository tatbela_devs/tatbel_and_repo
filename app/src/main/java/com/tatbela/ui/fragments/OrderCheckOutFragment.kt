package com.tatbela.ui.fragments


import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tatbela.R
import com.tatbela.ui.adapters.TestAdapter
import kotlinx.android.synthetic.main.fragment_order_check_out.*


class OrderCheckOutFragment : BaseFragment(), (Int) -> Unit {
    override fun invoke(p1: Int) {
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_order_check_out, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView_meals.isNestedScrollingEnabled=false
        recyclerView_meals.layoutManager = LinearLayoutManager(this.context)
        recyclerView_meals.adapter = TestAdapter(4, R.layout.order_meal_with_delete_list_item, this)
    }


}
