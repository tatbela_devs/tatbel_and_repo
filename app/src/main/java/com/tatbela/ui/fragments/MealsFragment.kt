package com.tatbela.ui.fragments


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.tatbela.Presenters.MealPresenter
import com.tatbela.R
import com.tatbela.api.Model
import com.tatbela.ui.adapters.CategoriesAdapter
import com.tatbela.ui.adapters.ChefWithMealsAdapter
import com.tatbela.utils.Constants
import kotlinx.android.synthetic.main.choose_category_layout.*
import kotlinx.android.synthetic.main.fragment_meals.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER


/**
 * A simple [Fragment] subclass.
 *
 */
class MealsFragment : BaseFragment(), (Model.Meal) -> Unit {
    override fun invoke(meal: Model.Meal) {
        val fragment = MealFragment()
        val bundle = Bundle()
        bundle.putSerializable(Constants.KEY_MEAL, meal)
        fragment.arguments = bundle
        changeFragment(fragment)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_meals, container, false)

    }


    private lateinit var presenter: MealPresenter

    private var preChosenCategory = -1

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        super.onViewCreated(view, savedInstanceState)
        if (arguments != null && arguments!!.containsKey(Constants.KEY_CATEGORY)) {
            preChosenCategory = arguments!!.getInt(Constants.KEY_CATEGORY)

        }

        presenter = MealPresenter(context!!)
        presenter.getCategories(::onGetCategories, ::onGetCategoriesError)
        recyclerView_all_meals.visibility = View.INVISIBLE

        recyclerView_all_meals_categories.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        recyclerView_all_meals.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        //recyclerView_all_meals.adapter = ExpandeableTestAdapter(this)

    }

    private fun onGetCategoriesError(throwable: Throwable) {
        Toast.makeText(context, throwable.message, Toast.LENGTH_SHORT).show()

    }

    private fun onGetCategories(arrayList: ArrayList<Model.CategoryWithIcon>) {
        val categoriesAdapter = CategoriesAdapter(arrayList, ::onCategoryClicked)
        recyclerView_all_meals_categories.adapter = categoriesAdapter
        if (preChosenCategory > -1) {
            categoriesAdapter.chosenPosition = preChosenCategory
            categoriesAdapter.notifyDataSetChanged()
            onCategoryClicked(preChosenCategory)

        }

    }

    private fun onCategoryClicked(i: Int) {
        cl_choose_category.visibility = View.INVISIBLE
        recyclerView_all_meals.visibility = View.VISIBLE
        presenter.getMealsByCategoryId(i, ::onGetMealsByCategory, ::onGetMealsByCategoryError)


    }

    private fun onGetMealsByCategoryError(throwable: Throwable) {

    }

    private lateinit var chefWithMeals: ArrayList<Model.ChefWithMeals>

    private fun onGetMealsByCategory(arrayList: ArrayList<Model.ChefWithMeals>) {
        this.chefWithMeals = arrayList
        recyclerView_all_meals.adapter = ChefWithMealsAdapter(arrayList, this)
    }


}
