package com.tatbela.ui.fragments

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tatbela.Presenters.AddressesPresenter
import com.tatbela.R
import com.tatbela.api.Model
import com.tatbela.ui.adapters.AddressAdapterWithData
import kotlinx.android.synthetic.main.fragment_addresses.*

class AddressesFragment : BaseFragment(), (Int) -> Unit {
    override fun invoke(p1: Int) {

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        // Inflate the layout for this fragment
        return inflater?.inflate(R.layout.fragment_addresses,
                container, false)
    }

    lateinit var presenter: AddressesPresenter
    fun onError(error: Throwable) {

    }

    private fun onGetAddresses(result: ArrayList<Model.AddressSwaggerWithId>) {


        recyclerView_addresses.adapter = AddressAdapterWithData(result, this)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        presenter = AddressesPresenter(context!!, ::onGetAddresses, ::onError)
        presenter.getAddresses()
        recyclerView_addresses.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        changeToolbarIconAction(R.drawable.ic_add_address, ::onAddAddressClicked)

    }

    private fun onAddAddressClicked(view: View) {
        changeFragment(AddAddressFragment())

    }
}