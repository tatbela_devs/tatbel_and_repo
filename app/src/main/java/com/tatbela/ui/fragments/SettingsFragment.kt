package com.tatbela.ui.fragments

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tatbela.R
import com.tatbela.api.Model
import com.tatbela.ui.activities.BecomeChefActivity
import com.tatbela.ui.adapters.SettingsMenuAdapter
import kotlinx.android.synthetic.main.fragment_settings.*


class SettingsFragment : BaseFragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_settings, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initNavigationView()

        tv_becomeChef.setOnClickListener {
            startActivity(Intent(context!!, BecomeChefActivity::class.java))
        }

    }

    private fun initNavigationView() {
        recyclerView_settings.layoutManager = LinearLayoutManager(context)
        recyclerView_settings.adapter = SettingsMenuAdapter(getItems(), ::onItemSelected)
    }

    private fun getItems(): ArrayList<Model.IconTitle> {
        val iconTitleItems = ArrayList<Model.IconTitle>()

        iconTitleItems.add(Model.IconTitle(R.drawable.ic_menu_my_profile, getString(R.string.my_profile)))
        iconTitleItems.add(Model.IconTitle(R.drawable.ic_menu_addresses, getString(R.string.my_addresses)))
        iconTitleItems.add(Model.IconTitle(R.drawable.ic_menu_language, getString(R.string.language)))
        iconTitleItems.add(Model.IconTitle(R.drawable.ic_menu_rate_app, getString(R.string.rate_app)))
        iconTitleItems.add(Model.IconTitle(R.drawable.ic_menu_privacy_terms, getString(R.string.privacy_terms)))
        iconTitleItems.add(Model.IconTitle(R.drawable.ic_menu_app_version, getString(R.string.app_version)))

        return iconTitleItems
    }

    private fun onItemSelected(i: Int) {
        when (i) {
            R.drawable.ic_menu_addresses -> {
                changeFragment(AddressesFragment())
            }
            R.drawable.ic_menu_my_profile->{
                changeFragment(MyProfileFragment())
            }
        }

    }


    companion object {

        @JvmStatic
        fun newInstance() = SettingsFragment()
    }
}
