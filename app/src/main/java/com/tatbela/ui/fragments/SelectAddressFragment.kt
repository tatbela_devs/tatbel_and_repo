package com.tatbela.ui.fragments


import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tatbela.R
import com.tatbela.ui.adapters.SelcectAddressAdapter
import kotlinx.android.synthetic.main.fragment_select_address.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
class SelectAddressFragment : BaseFragment(), (Int) -> Unit {
    override fun invoke(p1: Int) {
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_select_address, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView_addressesToSelectFrom.layoutManager=LinearLayoutManager(context)
        recyclerView_addressesToSelectFrom.adapter=SelcectAddressAdapter(this)
    }


}
