package com.tatbela.ui.fragments

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.tatbela.Presenters.NotificationsPresenter
import com.tatbela.R
import com.tatbela.api.Model
import com.tatbela.ui.adapters.NotificationAdapter
import kotlinx.android.synthetic.main.fragment_notifications.*

class NotificationsFragment : BaseFragment(), (Int) -> Unit {
    override fun invoke(p1: Int) {

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        // Inflate the layout for this fragment
        return inflater?.inflate(R.layout.fragment_notifications,
                container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val presenter = NotificationsPresenter(context!!)
        presenter.getNotifications(::onSuccess, ::onError)
        recyclerView_notifications.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
    }

    private fun onError(throwable: Throwable) {
        Toast.makeText(context, throwable.message, Toast.LENGTH_SHORT).show()

    }

    private fun onSuccess(list: List<Model.Notification>) {
        recyclerView_notifications.adapter = NotificationAdapter(list as ArrayList<Model.Notification>, this)

    }
}