package com.tatbela.ui.fragments

import android.content.Context
import android.location.Location
import android.location.LocationManager
import android.net.ConnectivityManager
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.tatbela.Managers.LocationAccessManager
import com.tatbela.Presenters.MealPresenter
import com.tatbela.R
import com.tatbela.api.Model
import com.tatbela.ui.adapters.BestMealAdapter
import com.tatbela.ui.adapters.MainScreenCategoriesAdapter
import com.tatbela.ui.adapters.TestAdapter
import com.tatbela.utils.Constants
import kotlinx.android.synthetic.main.best_meal.*
import kotlinx.android.synthetic.main.chef_today_meals.*
import kotlinx.android.synthetic.main.enable_location.*
import kotlinx.android.synthetic.main.from_to_dialog.*
import kotlinx.android.synthetic.main.home_fragment.*
import kotlinx.android.synthetic.main.no_connection.*
import kotlinx.android.synthetic.main.where_to_input.*


class HomeFragment : BaseFragment(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener, (Int) -> Unit {
    override fun invoke(p1: Int) {

    }

    override fun onMarkerClick(p0: Marker?): Boolean {
        cl_chef_today_meals.visibility = View.VISIBLE
        recyclerView_todays_meals.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        recyclerView_todays_meals.adapter = TestAdapter(R.layout.today_meal_list_item, this)
        iv_arrow_down.setOnClickListener {
            cl_chef_today_meals.visibility = View.GONE
        }
        return true
    }

    private var mMap: GoogleMap? = null
    var myLocation: LatLng? = null


    private var state: Int = 0


    private lateinit var chosenAddress: Model.AddressString

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            if (arguments!!.containsKey(Constants.KEY_ADDRESS_STRING)) {
                if (arguments!!.containsKey(Constants.KEY_ADDRESS_Type)) {
                    state = arguments!!.getInt(Constants.KEY_ADDRESS_Type)
                    chosenAddress = arguments!!.getSerializable(Constants.KEY_ADDRESS_STRING) as Model.AddressString
                }
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.home_fragment, container, false)
        // mapView!!.onCreate(savedInstanceState)
        // mapView!!.onResume()
        //MapsInitializer.initialize(getActivity())
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //if(!isMapReady) {
        map.onCreate(savedInstanceState)
        map.onResume()
        map.getMapAsync(this)
        if (isInternetConnected()) {
            cl_no_connection.visibility = View.GONE
            cl_best_meals.visibility = View.VISIBLE
            search_input.visibility = View.VISIBLE
        } else {
            cl_no_connection.visibility = View.VISIBLE
            cl_best_meals.visibility = View.GONE
            search_input.visibility = View.GONE

        }

        iv_gps.setOnClickListener {
            if (isLocationOn()) {
                onGetLocation(LocationAccessManager.location)
                search_input.visibility = View.VISIBLE
                cl_enable_location.visibility = View.GONE

            } else {
                cl_enable_location.visibility = View.VISIBLE
                cl_best_meals.visibility = View.GONE
                search_input.visibility = View.GONE
                cl_no_connection.visibility = View.GONE
            }
        }


        recyclerView_bestMeals.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        recyclerView_categories.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        ed_where_to.setOnClickListener { changeFragment(SearchLocationFragment()) }
        when (state) {
            Constants.TYPE_ADDRESS_FROM -> {
                cl_from_to.visibility = View.VISIBLE
                tv_fromLocation.text = chosenAddress.place
                tv_toLocation.text = "My Home"
                cl_best_meals.visibility = View.VISIBLE
            }
        }


        tv_more.setOnClickListener {
            changeFragment(MealsFragment())
        }

        val presenter = MealPresenter(context!!)
        presenter.geteBestMeals(::onSuccess, ::onError)
        presenter.getCategories(::onGetCategoriesSuccess, ::onGetCategoriesError)


    }

    private fun onError(throwable: Throwable) {

    }

    private fun onSuccess(arrayList: ArrayList<Model.MealSwagger>) {
        recyclerView_bestMeals.adapter = BestMealAdapter(arrayList, this)


    }


    private fun onGetCategoriesSuccess(arrayList: ArrayList<Model.CategoryWithIcon>) {
        recyclerView_categories.adapter = MainScreenCategoriesAdapter(arrayList, ::onCategoryClicked)

    }

    private fun onCategoryClicked(i: Int) {
        val mealsFragment=MealsFragment()
        val bundle = Bundle()
        bundle.putInt(Constants.KEY_CATEGORY,i)
        mealsFragment.arguments= bundle
        changeFragment(mealsFragment)

    }

    private fun onGetCategoriesError(throwable: Throwable) {

    }


    private fun isLocationOn(): Boolean {
        val locationManager: LocationManager = context?.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
    }

    private fun isInternetConnected(): Boolean {
        val conectivityManager: ConnectivityManager = context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        return conectivityManager.activeNetworkInfo != null

    }


    private var isMapReady: Boolean = false

    override fun onMapReady(googleMap: GoogleMap) {
        isMapReady = true
        mMap = googleMap
        //this.OnGetLocation(com.tatbela.Managers.LocationAccessManager.location)


    }

    fun onGetLocation(location: Location?) {
        if (mMap != null && location != null) {
            myLocation = LatLng(location!!.latitude, location!!.longitude)
            // mMap!!.addMarker(MarkerOptions().position(myLocation!!).title("My Location"))
            mMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(myLocation, 17f))

            val chefLocation = LatLng(location.latitude, location.longitude)
            putMarker(chefLocation)

            mMap!!.setOnMarkerClickListener(this)
        }
    }

    private fun putMarker(latLng: LatLng) {
        val markerOptions = MarkerOptions().position(latLng).title("Abdalla Elnaggar")
        val marker = mMap!!.addMarker(markerOptions)
        marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.img4))
    }
}