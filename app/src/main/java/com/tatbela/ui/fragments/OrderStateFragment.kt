package com.tatbela.ui.fragments


import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tatbela.R
import com.tatbela.ui.adapters.TestAdapter
import kotlinx.android.synthetic.main.fragment_order_edit.*


class OrderStateFragment : BaseFragment(), (Int) -> Unit {
    override fun invoke(p1: Int) {
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_order_state, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView_meals.isNestedScrollingEnabled = false
        recyclerView_meals.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        recyclerView_meals.adapter = TestAdapter(2, R.layout.order_meal_list_item, this)
        showAddReviewDialog()
    }

    private fun showReviewSentDialog() {
        var builder: AlertDialog.Builder = AlertDialog.Builder(context!!)
        val view = LayoutInflater.from(context).inflate(R.layout.dialog_review_sent, null)
        builder.setView(view)
        val dialog = builder.create()
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.show()
    }
    private fun showAddReviewDialog() {
        var builder: AlertDialog.Builder = AlertDialog.Builder(context!!)
        val view = LayoutInflater.from(context).inflate(R.layout.dialog_submit_review, null)
        builder.setView(view)
        val dialog = builder.create()
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.show()
    }


}
