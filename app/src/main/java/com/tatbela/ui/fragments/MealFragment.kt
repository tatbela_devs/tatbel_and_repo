package com.tatbela.ui.fragments

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.bumptech.glide.Glide
import com.tatbela.Presenters.MealPresenter
import com.tatbela.R
import com.tatbela.api.Model
import com.tatbela.ui.adapters.ReviewsAdapter
import com.tatbela.utils.Constants
import kotlinx.android.synthetic.main.chef_icon_address.*
import kotlinx.android.synthetic.main.fragment_meal.*

class MealFragment : BaseFragment(), (Int) -> Unit {
    override fun invoke(p1: Int) {

    }

    private var container: ViewGroup? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        this.container = container
        return inflater.inflate(R.layout.fragment_meal, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        recyclerView_reviews.isNestedScrollingEnabled = false
        recyclerView_reviews.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        changeToolbarIconAction(R.drawable.ic_add_to_cart, ::onAddToCartClicked)
        val presenter = MealPresenter(context!!)
        if (arguments != null && arguments!!.containsKey(Constants.KEY_MEAL)) {
            val meal = arguments!!.getSerializable(Constants.KEY_MEAL) as Model.Meal

            presenter.getMealById(meal.id, ::onSuccess, ::onError)
        }


    }

    private fun onAddToCartClicked(view: View) {
        addToCart()
    }

    private fun addToCart() {
        showAddedToCartDialog()
    }

    private fun showAddedToCartDialog() {
        var builder: AlertDialog.Builder = AlertDialog.Builder(context!!)
        val view = LayoutInflater.from(context).inflate(R.layout.dialog_added_to_your_cart, null)
        builder.setView(view)
        val dialog = builder.create()
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.show()
    }

    fun onError(throwable: Throwable) {
        Toast.makeText(context, throwable.message, Toast.LENGTH_SHORT).show()

    }

    fun onSuccess(mealWithReviews: Model.MealWithReviews) {
        recyclerView_reviews.adapter = ReviewsAdapter(mealWithReviews.reviews as ArrayList<Model.Review>, this)
        tv_chef_name.text = mealWithReviews.chiefName
        tv_chefName.text = mealWithReviews.chiefName
        tv_mealDescription.text = mealWithReviews.description
        tv_mealPrice.text = mealWithReviews.price.toString()
        tv_mealType.text = mealWithReviews.category!!.name
        Glide.with(context!!).load(mealWithReviews.images!![0]).into(iv_mealPic)
        tv_time.text = mealWithReviews.prepareTime
        tv_chef_address.text = mealWithReviews.chief!!.address!!.description
        rb_meal_rate.rating = mealWithReviews.reviews!![0].rating.toFloat()


    }
}


