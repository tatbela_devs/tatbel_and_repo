package com.tatbela.ui.fragments


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tatbela.R
import com.tatbela.api.Model
import com.tatbela.ui.adapters.TestAdapter
import com.tatbela.utils.Constants
import kotlinx.android.synthetic.main.fragment_search_location.*


/**
 * A simple [Fragment] subclass.
 */
class SearchLocationFragment : BaseFragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_search_location, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView_results.layoutManager=LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false)
        recyclerView_results.adapter=TestAdapter(R.layout.location_result_list_item,::onItemClicked)

    }

    private fun onItemClicked(i: Int) {
        var addrees  = Model.AddressString("Somoha Ebrahimia","02 km")
        publishChosenAddress(Constants.TYPE_ADDRESS_FROM,addrees)



    }

}// Required empty public constructor
