package com.tatbela.ui.fragments

import android.location.Location
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.View
import com.tatbela.Managers.ILocation
import com.tatbela.api.Model
import com.tatbela.ui.activities.MainActivity
import com.tatbela.utils.Constants


/**
 * Created by Daniel Tadrous on 5/26/2018.
 */

open class BaseFragment : Fragment(), ILocation {
    private lateinit var mainActivity: MainActivity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainActivity = activity as MainActivity

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.isClickable=true
        view.isFocusable=true
    }

    override fun OnGetLocation(location: Location?) {

    }

    fun changeFragment(fragment: BaseFragment) {
        mainActivity.changeFragment(fragment)
    }

    fun publishChosenAddress(type: Int, address: Model.AddressString) {
        mainActivity.chosenAddress = address
        mainActivity.addressType = Constants.TYPE_ADDRESS_FROM
        val fragment = HomeFragment()
        val bundle = Bundle()
        bundle.putSerializable(Constants.KEY_ADDRESS_STRING, address)
        bundle.putInt(Constants.KEY_ADDRESS_Type, Constants.TYPE_ADDRESS_FROM)
        fragment.arguments = bundle
        changeFragment(fragment)

    }

    fun changeToolbarIconAction(icon: Int, onClick: (View) -> Unit) {
        mainActivity.changeToolbarIconAction(icon, onClick)

    }
    fun hideToolbarIcon(){
        mainActivity.hideToolbarIcon()
    }

}