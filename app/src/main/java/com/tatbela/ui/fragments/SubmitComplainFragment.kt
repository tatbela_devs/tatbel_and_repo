package com.tatbela.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.tatbela.Presenters.ComplainPresenter
import com.tatbela.R
import com.tatbela.api.Model
import com.tatbela.utils.Constants
import kotlinx.android.synthetic.main.fragment_submit_complain.*

class SubmitComplainFragment : BaseFragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_submit_complain, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val presenter = ComplainPresenter(context!!)
        bt_submitComplain.setOnClickListener {
            presenter.submitComplain(::onSuccess, ::onError)
        }
    }

    private fun onError(throwable: Throwable) {
        Toast.makeText(context, throwable.message, Toast.LENGTH_SHORT).show()
    }

    private fun onSuccess(responseSwagger: Model.ResponseSwagger) {
        if (responseSwagger.status == Constants.STATUS_SUCCESS) {
            Toast.makeText(context, "Complain Submitted Successful", Toast.LENGTH_SHORT).show()
        }

    }
}