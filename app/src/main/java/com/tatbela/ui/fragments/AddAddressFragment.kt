package com.tatbela.ui.fragments

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tatbela.R
import kotlinx.android.synthetic.main.fragment_add_address.*

class AddAddressFragment : BaseFragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        // Inflate the layout for this fragment
        return inflater?.inflate(R.layout.fragment_add_address,
                container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        hideToolbarIcon()
        btn_addAddress.setOnClickListener {
            showAddressAddedDialog()
        }

    }

    private fun showAddressAddedDialog() {
        var builder: AlertDialog.Builder = AlertDialog.Builder(context!!)
        val view = LayoutInflater.from(context).inflate(R.layout.dialog_address_added, null)
        builder.setView(view)
        val dialog = builder.create()
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.show()
    }


}