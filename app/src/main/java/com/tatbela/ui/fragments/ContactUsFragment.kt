package com.tatbela.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tatbela.R

class ContactUsFragment : BaseFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
         // Inflate the layout for this fragment
        return inflater?.inflate(R.layout.contact_us_fragment,
                container, false)
    }
}