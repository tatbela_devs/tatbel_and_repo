package com.tatbela.ui.fragments


import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tatbela.Presenters.ComplainPresenter

import com.tatbela.R
import com.tatbela.api.Model
import com.tatbela.ui.adapters.CommentsAdapter
import com.tatbela.utils.Constants
import kotlinx.android.synthetic.main.fragment_my_complain.*


class MyComplainFragment : BaseFragment(), (Int) -> Unit {
    override fun invoke(p1: Int) {
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_complain, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView_comments.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        recyclerView_comments.isNestedScrollingEnabled = false
        val presenter = ComplainPresenter(context!!)
        if (arguments != null && arguments!!.containsKey(Constants.KEY_COMPLAIN_ID)) {
            val id = arguments!!.getInt(Constants.KEY_COMPLAIN_ID)
            presenter.getComplainById("" + id, ::onSuccess, ::onError)


        }

    }

    private fun onError(throwable: Throwable) {

    }

    private fun onSuccess(complainWithComments: Model.ComplainWithComments) {
        tv_complainDate.text = complainWithComments.date
        tv_complainTitle.text = complainWithComments.title
        tv_complainMessage.text = complainWithComments.description

        recyclerView_comments.adapter = CommentsAdapter(complainWithComments.comments!!)

    }


}
