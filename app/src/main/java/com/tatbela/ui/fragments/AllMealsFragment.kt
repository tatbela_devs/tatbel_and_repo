package com.tatbela.ui.fragments

import android.os.Bundle
import android.support.v4.app.FragmentManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tatbela.R
import com.tatbela.ui.activities.MainActivity
import kotlinx.android.synthetic.main.all_meals_fragment.*

class AllMealsFragment : BaseFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
         // Inflate the layout for this fragment
        val view = inflater?.inflate(R.layout.all_meals_fragment,
                container, false)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        button.setOnClickListener {
            var fragmentManager: FragmentManager? = activity?.supportFragmentManager
            var mealDetailsFragment = MealFragment()
            fragmentManager?.beginTransaction()?.replace(R.id.content_frame, mealDetailsFragment)?.commit()
            (activity as MainActivity).currentFragment = mealDetailsFragment
            activity?.invalidateOptionsMenu()
        }
    }
}