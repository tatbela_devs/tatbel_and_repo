package com.tatbela.ui.fragments

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import com.tatbela.Presenters.ChefPresenter
import com.tatbela.Presenters.CountryPresenter
import com.tatbela.R
import com.tatbela.api.Model
import com.tatbela.ui.adapters.ChefsWithRateAdapter
import com.tatbela.utils.Constants
import kotlinx.android.synthetic.main.fragment_our_cheifs.*

class OurCheifsFragment : BaseFragment(), (Int) -> Unit, AdapterView.OnItemSelectedListener {
    override fun onNothingSelected(parent: AdapterView<*>?) {
        val arrayAdapter = ArrayAdapter<String>(context!!, android.R.layout.simple_list_item_1, getAreasAsString(country!!.cities!![0]!!.areas))
        sp_areas.adapter=arrayAdapter

    }

    private fun  getAreasAsString(areas: List<Model.Area?>?): ArrayList<String> {
        val stringArrayList = ArrayList<String>()
        for(area in areas!!){
            stringArrayList.add(area!!.name!!)

        }
        return stringArrayList
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val arrayAdapter = ArrayAdapter<String>(context!!, android.R.layout.simple_list_item_1, getAreasAsString(country!!.cities!![position]!!.areas))
        sp_areas.adapter=arrayAdapter
    }

    override fun invoke(p1: Int) {

        val fragment = ChefFragment()
       val bundle=Bundle()
        bundle.putSerializable(Constants.KEY_CHEF_WITH_RATE,chefsWithRate[p1])
        fragment.arguments=bundle

        changeFragment(fragment)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        // Inflate the layout for this fragment
        return inflater?.inflate(R.layout.fragment_our_cheifs,
                container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val presenter = ChefPresenter(context!!)
        val generalInfoPresenter=CountryPresenter(context!!)
        generalInfoPresenter.getCountryById(""+1,::onGetCountrySuccess,::onGetCountryError)
        presenter.getOurChefs(1, 2, 3, ::onGetOurChefsSuccess, ::onGetOurChefsError)
        cl_areas.setOnClickListener {
            sp_areas.performClick()
        }
        cl_provinces.setOnClickListener {
            sp_provinces.performClick()
        }
        recyclerView_chefList.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)

    }

    private fun onGetCountryError(throwable: Throwable) {
        Toast.makeText(context,throwable.message,Toast.LENGTH_SHORT).show()


    }

    private lateinit var country: Model.Country

    private fun onGetCountrySuccess(country: Model.Country) {
        this.country=country
        val arrayAdapter = ArrayAdapter<String>(context!!, android.R.layout.simple_list_item_1, getCitiesAsString(country.cities))
        sp_provinces.adapter= arrayAdapter
        sp_provinces.onItemSelectedListener=this

    }

    private fun getCitiesAsString(cities: List<Model.City?>?): ArrayList<String> {
        val stringArrayList = ArrayList<String>()
        for(city in cities!!){
            stringArrayList.add(city!!.name!!)

        }
        return stringArrayList



    }

    private fun onGetOurChefsError(throwable: Throwable) {
        Toast.makeText(context,throwable.message,Toast.LENGTH_SHORT).show()

    }

    private lateinit var chefsWithRate: ArrayList<Model.ChefWithRate>

    private fun onGetOurChefsSuccess(arrayList: ArrayList<Model.ChefWithRate>) {
        chefsWithRate=arrayList
        recyclerView_chefList.adapter = ChefsWithRateAdapter(arrayList, this)


    }
}