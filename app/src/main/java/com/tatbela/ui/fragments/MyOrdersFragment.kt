package com.tatbela.ui.fragments

import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tatbela.R
import com.tatbela.ui.adapters.TestAdapter
import kotlinx.android.synthetic.main.my_orders_fragment.*
import kotlinx.android.synthetic.main.my_orders_fragment.view.*

class MyOrdersFragment : BaseFragment(), (Int) -> Unit {
    override fun invoke(p1: Int) {
        changeFragment(OrderStateFragment())
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        // Inflate the layout for this fragment
        return inflater?.inflate(R.layout.my_orders_fragment,
                container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView_items.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        ll_currentOrders.setOnClickListener(::onHeaderItemClicked)
        ll_historyOrders.setOnClickListener(::onHeaderItemClicked)
        recyclerView_items.adapter = TestAdapter(R.layout.order_list_item, this)

    }

    private fun onHeaderItemClicked(v: View) {
        uncheckAll()
        when (v.id) {
            R.id.ll_currentOrders -> {
                ll_currentOrders.setBackgroundResource(R.drawable.button_curved8dp_green_bg)
                ll_currentOrders.tv_currentOrders.setTextColor(ContextCompat.getColor(context!!, R.color.white))
                recyclerView_items.adapter = TestAdapter(10,R.layout.order_list_item, this)


            }
            R.id.ll_historyOrders -> {
                ll_historyOrders.setBackgroundResource(R.drawable.button_curved8dp_green_bg)
                ll_historyOrders.tv_history.setTextColor(ContextCompat.getColor(context!!, R.color.white))
                recyclerView_items.adapter = TestAdapter(3,R.layout.order_list_item, this)

            }

        }
    }

    private fun uncheckAll() {
        ll_currentOrders.tv_currentOrders.setTextColor(ContextCompat.getColor(context!!, R.color.warm_gray))
        ll_currentOrders.setBackgroundResource(R.drawable.meal_item_bg)
        ll_historyOrders.tv_history.setTextColor(ContextCompat.getColor(context!!, R.color.warm_gray))
        ll_historyOrders.setBackgroundResource(R.drawable.meal_item_bg)


    }

}