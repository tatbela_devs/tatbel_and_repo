package com.tatbela.ui.fragments

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.tatbela.R
import kotlinx.android.synthetic.main.my_profile_fragment.*

class MyProfileFragment : BaseFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        // Inflate the layout for this fragment
        return inflater?.inflate(R.layout.my_profile_fragment,
                container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        btn_save.setOnClickListener { showSaveDialog() }
        btn_changePassword.setOnClickListener { showChangePasswordDialog() }
        tv_paymentDetails.setOnClickListener { changeFragment(MyProfilePaymentFragment()) }
    }

    private fun showChangePasswordDialog() {
        var builder: AlertDialog.Builder = AlertDialog.Builder(context!!)
        val view = LayoutInflater.from(context).inflate(R.layout.dialog_change_password, null)
        builder.setView(view)
        val dialog = builder.create()
        val change=view.findViewById<Button>(R.id.btn_send)
        change.setOnClickListener { dialog.dismiss()
        sendChangePasswordRequest()}
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.show()
    }

    private fun sendChangePasswordRequest() {
        if(true){
            showPasswordDidntChangedDialog()
        }else{

            showPasswordChangedDialog()

        }

    }

    private fun showPasswordDidntChangedDialog() {
            val builder: AlertDialog.Builder = AlertDialog.Builder(context!!)
            val view = LayoutInflater.from(context).inflate(R.layout.dialog_password_didnt_changed, null)
            builder.setView(view)
            val dialog = builder.create()
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.show()

    }

    private fun showPasswordChangedDialog() {
        val builder: AlertDialog.Builder = AlertDialog.Builder(context!!)
        val view = LayoutInflater.from(context).inflate(R.layout.dialog_password_changed, null)
        builder.setView(view)
        val dialog = builder.create()
        val cancelBtn=view.findViewById<Button>(R.id.btn_review_meals)
        cancelBtn.setOnClickListener { dialog.dismiss()}
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.show()
    }

    private fun showSaveDialog() {
        var builder: AlertDialog.Builder = AlertDialog.Builder(context!!)
        val view = LayoutInflater.from(context).inflate(R.layout.dialog_your_information_updated, null)
        builder.setView(view)
        val dialog = builder.create()
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.show()
    }
}