package com.tatbela.ui.fragments


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.bumptech.glide.Glide
import com.tatbela.Presenters.ChefPresenter
import com.tatbela.R
import com.tatbela.api.Model
import com.tatbela.ui.adapters.ChefMealsAdapter
import com.tatbela.ui.adapters.ReviewsAdapter
import com.tatbela.utils.Constants
import kotlinx.android.synthetic.main.chef_header_layout.*
import kotlinx.android.synthetic.main.fragment_chef.*


/**
 * A simple [Fragment] subclass.
 *
 */
class ChefFragment : BaseFragment(), (Int) -> Unit {
    override fun invoke(p1: Int) {
        changeFragment(MealFragment())

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_chef, container, false)
    }

    private lateinit var chefWithRate: Model.ChefWithRate

    private lateinit var presenter: ChefPresenter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView_chefScreenList.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        tv_full_menu.setOnClickListener(::onHeaderItemClicked)
        tv_todays_meals.setOnClickListener(::onHeaderItemClicked)
        tv_reviews.setOnClickListener(::onHeaderItemClicked)
        presenter = ChefPresenter(context!!)

        if (arguments != null && arguments!!.containsKey(Constants.KEY_CHEF_WITH_RATE)) {
            chefWithRate = arguments!!.getSerializable(Constants.KEY_CHEF_WITH_RATE) as Model.ChefWithRate
            tv_chef_name.text = chefWithRate.name
            tv_chef_address.text = chefWithRate.address!!.description
            Glide.with(context!!).load(chefWithRate.image).into(iv_chef_icon)
            rb_chef_rate.rating = chefWithRate.rating.toFloat()
            presenter.getChefById(chefWithRate.id, ::onGetChefSuccess, ::onError)
        }


    }

    fun onError(error: Throwable) {

    }


    private fun onGetChefSuccess(result: Model.ChefWithRate) {
        chefWithRate=result
        onHeaderItemClicked(tv_full_menu)

    }

    private fun onHeaderItemClicked(v: View) {
        uncheckAll()
        v.setBackgroundResource(R.color.asparagus)
        (v as TextView).setTextColor(ContextCompat.getColor(context!!, R.color.white))
        when (v.id) {
            R.id.tv_todays_meals -> {
                tv_listHint.visibility = View.VISIBLE
                tv_listHint.text = getString(R.string.available_today)
                tv_listHint.setTextColor(ContextCompat.getColor(context!!, R.color.pinkishGrey))
                recyclerView_chefScreenList.adapter = ChefMealsAdapter(filterByToday(chefWithRate.meals), this)



            }
            R.id.tv_full_menu -> {
                tv_listHint.visibility = View.VISIBLE
                tv_listHint.text = getString(R.string.upon_request)
                tv_listHint.setTextColor(ContextCompat.getColor(context!!, android.R.color.holo_red_dark))
                recyclerView_chefScreenList.adapter = ChefMealsAdapter(chefWithRate.meals, this)



            }
            R.id.tv_reviews -> {
                recyclerView_chefScreenList.adapter = ReviewsAdapter(chefWithRate.reviews as ArrayList<Model.Review>, ::onReviewClicked)
                tv_listHint.visibility = View.GONE

            }
        }
    }

    private fun filterByToday(meals: List<Model.ChefMeal>?): List<Model.ChefMeal>? {
        val todayMeals = ArrayList<Model.ChefMeal>()
        for(meal in meals!!){
            if(meal.isIsToday){
                todayMeals.add(meal)
            }

        }
        return todayMeals
    }

    private fun onReviewClicked(i: Int) {

    }

    private fun uncheckAll() {
        uncheck(tv_todays_meals)
        uncheck(tv_full_menu)
        uncheck(tv_reviews)

    }

    private fun uncheck(tv: TextView?) {
        tv!!.setBackgroundResource(android.R.color.transparent)
        tv.setTextColor(ContextCompat.getColor(context!!, R.color.asparagus))


    }


}
